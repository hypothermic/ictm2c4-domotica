#!/usr/bin/env zsh
# Script om een docker image te bouwen van SensorHost en lokaal te runnen.

cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 || exit
docker build . --tag=ictm2c4/rpisensorhost:latest --build-arg JAR_FILE=./build/libs/rpisensorhost-10.0.0.1.jar
docker run --name=rpisensorhost --publish=8080:8080 ictm2c4/rpisensorhost:latest