package org.ictm2c4.rpisensorhost.rest;

import org.ictm2c4.rpisensorhost.domain.meting.LuchtvochtigheidMeting;
import org.ictm2c4.rpisensorhost.domain.meting.TemperatuurMeting;
import org.ictm2c4.rpisensorhost.domain.sensor.LuchtvochtigheidSensor;
import org.ictm2c4.rpisensorhost.domain.sensor.TemperatuurSensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/luchtvochtigheid")
public class LuchtvochtigheidController {

    @Autowired
    @Qualifier("luchtvochtigheidSensor")
    private LuchtvochtigheidSensor luchtvochtigheidSensor;

    @RequestMapping(value = "",
                    method = RequestMethod.GET,
                    produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody LuchtvochtigheidMeting getLuchtvochtigheid() {
        return luchtvochtigheidSensor.verrichtMeting();
    }
}
