package org.ictm2c4.rpisensorhost.rest;

import org.ictm2c4.rpisensorhost.domain.meting.TemperatuurMeting;
import org.ictm2c4.rpisensorhost.domain.sensor.TemperatuurSensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/temperatuur")
public class TemperatuurController {

    @Autowired
    @Qualifier("temperatuurSensor")
    private TemperatuurSensor temperatuurSensor;

    @RequestMapping(value = "",
                    method = RequestMethod.GET,
                    produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody TemperatuurMeting getTemperatuur() {
        return temperatuurSensor.verrichtMeting();
    }
}
