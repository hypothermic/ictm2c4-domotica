package org.ictm2c4.rpisensorhost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensorHostApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensorHostApplication.class, args);
    }

}
