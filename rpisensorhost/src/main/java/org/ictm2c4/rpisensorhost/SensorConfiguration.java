package org.ictm2c4.rpisensorhost;

import de.larsgrefer.sense_hat.SenseHat;
import org.ictm2c4.rpisensorhost.domain.sensor.LuchtvochtigheidSensor;
import org.ictm2c4.rpisensorhost.domain.sensor.TemperatuurSensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@Configuration
public class SensorConfiguration {

    @Autowired
    private Environment environment;

    private SenseHat senseHat;

    @PostConstruct
    public void init() throws Exception {
        // Ik heb nogal problemen gehad met deze library omdat hij de "frame buffer"
        // niet kon vinden... daarom een optie om deze uit te schakelen.
        if (environment.containsProperty("useFrameBuffer")
                && environment.getProperty("useFrameBuffer", Boolean.class)) {
            senseHat = new SenseHat();
        } else {
            senseHat = new SenseHat(null);
        }
    }

    @Bean
    public SenseHat getSenseHat() {
        return senseHat;
    }

    @Bean
    public TemperatuurSensor getTemperatuurSensor() {
        return new TemperatuurSensor(senseHat);
    }

    @Bean
    public LuchtvochtigheidSensor getLuchtvochtigheidSensor() {
        return new LuchtvochtigheidSensor(senseHat);
    }
}
