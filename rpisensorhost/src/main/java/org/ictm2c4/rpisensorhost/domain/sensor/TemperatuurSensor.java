package org.ictm2c4.rpisensorhost.domain.sensor;

import de.larsgrefer.sense_hat.SenseHat;
import lombok.AllArgsConstructor;
import org.ictm2c4.rpisensorhost.domain.meting.TemperatuurMeting;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TemperatuurSensor implements Sensor {

    private SenseHat senseHat;

    @Override
    public TemperatuurMeting verrichtMeting() {
        return TemperatuurMeting.builder()
                                    .temperatuur(this.senseHat.getTemperature())
                                    .build();
    }
}
