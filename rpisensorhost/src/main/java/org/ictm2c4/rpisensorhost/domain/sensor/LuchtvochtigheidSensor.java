package org.ictm2c4.rpisensorhost.domain.sensor;

import de.larsgrefer.sense_hat.SenseHat;
import lombok.AllArgsConstructor;
import org.ictm2c4.rpisensorhost.domain.meting.LuchtvochtigheidMeting;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class LuchtvochtigheidSensor implements Sensor {

    private SenseHat senseHat;

    @Override
    public LuchtvochtigheidMeting verrichtMeting() {
        return LuchtvochtigheidMeting.builder()
                                        .luchtvochtigheid(this.senseHat.getHumidity())
                                        .build();
    }
}
