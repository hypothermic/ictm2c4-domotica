package org.ictm2c4.rpisensorhost.domain.meting;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LuchtvochtigheidMeting implements Meting {

    private double luchtvochtigheid;

    @Override
    public double getWaarde() {
        return luchtvochtigheid;

        // comment
    }
}
