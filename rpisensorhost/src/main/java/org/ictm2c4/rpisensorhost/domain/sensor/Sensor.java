package org.ictm2c4.rpisensorhost.domain.sensor;

import org.ictm2c4.rpisensorhost.domain.meting.Meting;

public interface Sensor {

    Meting verrichtMeting();

}
