#!/usr/bin/env zsh
# Script om SensorHost te builden en uploaden naar Docker Hub.
# Let op, log eerst in met `docker login`

DOCKER_GEBRUIKERSNAAM="hypothermic"

cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 || exit
docker build . --tag=ictm2c4/rpisensorhost:latest --build-arg JAR_FILE=./build/libs/rpisensorhost-10.0.0.1.jar
docker tag ictm2c4/rpisensorhost ${DOCKER_GEBRUIKERSNAAM}/rpisensorhost
docker push ${DOCKER_GEBRUIKERSNAAM}/rpisensorhost