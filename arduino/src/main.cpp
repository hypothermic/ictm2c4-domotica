#include <Arduino.h>
#include <SPI.h>

/**
 * Laat een nummer zien op het 7-segment display.<br />
 * <br />
 * Eigenlijk is het 8-segment display want er is ook nog een punt.
 *
 * @param nummer Nummer tussen de 0-9
 */
static void displayNumber(uint8_t nummer);

/**
 * Wacht tot er een byte beschikbaar is op de serial en lees de byte.
 *
 * @return uint8_t 8-bit unsigned int met de data van de byte die gelezen werd
 */
static uint8_t serialWaitAndRead();

/**
 * De pin mapping
 */
enum Pins : uint8_t {

    /**
     * Rode LED
     */
    PIN_LED_R   = 6,

    /**
     * Groene LED
     */
    PIN_LED_G   = 5,

    /**
     * Segment A van het 7-seg display
     */
    PIN_SEG_A   = 7,

    /**
     * Segment B van het 7-seg display
     */
    PIN_SEG_B   = 8,

    /**
     * Segment C van het 7-seg display
     */
    PIN_SEG_C   = 9,

    /**
     * Segment D van het 7-seg display
     */
    PIN_SEG_D   = 10,

    /**
     * Segment E van het 7-seg display
     */
    PIN_SEG_E   = 11,

    /**
     * Segment F van het 7-seg display
     */
    PIN_SEG_F   = 12,

    /**
     * Segment G van het 7-seg display
     */
    PIN_SEG_G   = 13,

    /**
     * De linker button
     */
    PIN_BTN_L   = 2,

    /**
     * De middelste button
     */
    PIN_BTN_M   = 3,

    /**
     * De rechter button
     */
    PIN_BTN_R   = 4,

    /**
     * De piezo output
     */
    PIN_PIEZO   = PIN_A0,

    /**
     * De LDR input
     */
    PIN_LDR     = PIN_A1,

    /**
     * De MISO pin van de SD kaart lezer
     */
    PIN_SD_MISO = PIN_A2,

    /**
     * De clock pin van de SD kaart lezer
     */
    PIN_SD_SCK  = PIN_A3,

    /**
     * De MOSI pin van de SD kaart lezer
     */
    PIN_SD_MOSI = PIN_A4,

    /**
     * De CS pin van de SD kaart lezer
     */
    PIN_SD_CS   = PIN_A5,

};

/**
 * Welk commando bij welk nummer hoort
 */
enum Commands : uint8_t {

    VERWARMING_AAN = 101,
    VERWARMING_UIT = 102,

    /**
     * Na dit commando volgt een byte met het nummer (0-9)
     * dat op de 7-segment display getoond moet worden.
     */
    DISPLAY_NUMMER = 103,

    MUSIC_PAUSE    = 104,
    MUSIC_RESUME   = 105,

    /**
     * Dit commando wordt gebruikt om de toonhoogtes en
     * toonlengtes van het muzieknummer naar de Arduino
     * te sturen. Na het commando volgt een unsigned byte
     * met de lengte van het nummer, een array van shorts
     * met de toonhoogtes en een array van bytes met de
     * toonlengtes. Beide arrays moeten de grootte van
     * het lengte van het nummer hebben.
     */
    MUSIC_TRANSFER = 106,

    LIGHTING_AAN   = 107,
    LIGHTING_UIT   = 108,

    LIGHT_REQUEST  = 109,

};

/**
 * Array met alle pins die naar OUTPUT mode moeten worden gezet
 */
const uint8_t OUTPUT_PINS[] = {
        PIN_LED_R,
        PIN_LED_G,
        PIN_SEG_A,
        PIN_SEG_B,
        PIN_SEG_C,
        PIN_SEG_D,
        PIN_SEG_E,
        PIN_SEG_F,
        PIN_SEG_G,
        PIN_PIEZO,
        PIN_SD_MISO,
        PIN_SD_SCK,
        PIN_SD_MOSI,
        PIN_SD_CS,
};

/**
 * Array met alle pins die naar INPUT mode moeten worden gezet
 */
const uint8_t INPUT_PINS[] = {
        PIN_BTN_L,
        PIN_BTN_M,
        PIN_BTN_R,
        PIN_LDR,
};

/**
 * Array met alle 7-segmentdisplay pins
 */
const uint8_t SEG_PINS[]   = {
        PIN_SEG_A,
        PIN_SEG_B,
        PIN_SEG_C,
        PIN_SEG_D,
        PIN_SEG_E,
        PIN_SEG_F,
        PIN_SEG_G
};

/**
 * Lengtes van de arrays
 */
enum ArrayLengths : uint8_t {

    /**
     * Lengte van de SEG_PINS array
     */
    SEG_PINS_LEN      = sizeof(SEG_PINS)    / sizeof(SEG_PINS[0]),

    /**
     * Lengte van de OUTPUT_PINS array
     */
    OUTPUT_PINS_LEN   = sizeof(OUTPUT_PINS) / sizeof(OUTPUT_PINS[0]),

    /**
     * Lengte van de INPUT_PINS array
     */
    INPUT_PINS_LEN    = sizeof(INPUT_PINS)  / sizeof(INPUT_PINS[0]),

};

/**
 * Een array van tonen (niet null-terminated, zie song_length voor lengte)
 */
uint16_t     *song_notes        = nullptr;

/**
 * Een array van toonlengtes (niet null-terminated, zie song_length voor lengte)
 */
uint8_t      *song_durations    = nullptr;

/**
 * Hoeveel tonen het huidige nummer heeft.
 */
uint8_t      song_length       = 0;

/**
 * De huidige toon die afgespeeld wordt.
 */
int           currentNote       = 0;

/**
 * Tijd in milliseconden wanneer de laatste toon afgespeeld was.
 */
uint32_t      millisLastNote    = 0;

/**
 * Tijd in milliseconden wanneer de lichtintensiteit voor het laatst verstuurd was.
 */
uint32_t      millisLastLight   = 0;

bool          isPaused          = false;

/**
 * Functie die eenmaal wordt aangeroepen als de Arduino start.
 */
void setup() {
    // Zet de pin mode van elke output pin naar OUTPUT
    for (int i = 0; i < OUTPUT_PINS_LEN; ++i) {
        pinMode(OUTPUT_PINS[i], OUTPUT);
    }

    // Zet de pin mode van elke input pin naar INPUT
    for (int i = 0; i < INPUT_PINS_LEN; ++i) {
        pinMode(INPUT_PINS[i], INPUT);
    }

    // Initialiseer de seriele verbinding
    Serial.begin(9600);
}

/**
 * Functie die met elke Arduino loop herhaald wordt.
 */
void loop() {
    /**
     * @brief Milliseconden na het opstarten van de Arduino.
     */
    unsigned long currentMillis = millis();

    // Heeft de centrale applicatie informatie gestuurd?
    if (Serial.available()) {
        switch (Serial.read()) {
            // De centrale app heeft "101" gestuurd, wat betekent dat de verwarming aan moet
            case VERWARMING_AAN:
                digitalWrite(PIN_LED_R, HIGH);
                break;
            // De centrale app heeft "102" gestuurd, wat betekent dat de verwarming uit moet
            case VERWARMING_UIT:
                digitalWrite(PIN_LED_R, LOW);
                break;
            // De centrale app heeft "103" gestuurd, wat betekent dat het nummer op de 7-segment display veranderd moet worden
            case DISPLAY_NUMMER: {
                // Wacht op de extra data
                while (!Serial.available());

                // Lees de data. Dit is het nummer dat op de 7-segmentdisplay moet worden getoond.
                uint8_t nummer = Serial.read();

                // Toon het nummer
                displayNumber(nummer);

                break;
            }
            // De centrale app heeft "104" gestuurd, wat betekent dat de muziek gepauzeerd moet worden
            case MUSIC_PAUSE:
                isPaused = true;
                break;
            // De centrale app heeft "105" gestuurd, wat betekent dat de muziek hervat moet worden
            case MUSIC_RESUME:
                isPaused = false;
                break;
            // De centrale app heeft "106" gestuurd, wat betekent dat een nieuw muzieknummer geladen moet worden
            case MUSIC_TRANSFER: {
                // Verwijder het huidige muzieknummer uit het geheugen (indien het aangewezen is)
                if (song_notes) {
                    free(song_notes);
                }
                if (song_durations) {
                    free(song_durations);
                }

                // Lees de lengte van het nummer
                song_length = serialWaitAndRead() & 0xFF;

                // TODO Check of hier genoeg heap ruimte voor is...
                song_notes = new uint16_t[song_length];
                song_durations = new uint8_t[song_length];

                for (int i = 0; i < song_length; i++) {
                    song_notes[i] = serialWaitAndRead() & 0xFF;
                    song_notes[i] |= (serialWaitAndRead() << 8);
                }

                for (int i = 0; i < song_length; i++) {
                    song_durations[i] = serialWaitAndRead() & 0xFF;
                }

                break;
            }
            // De centrale app heeft "101" gestuurd, wat betekent dat de verwarming aan moet
            case LIGHTING_AAN:
                digitalWrite(PIN_LED_G, HIGH);
                break;
            // De centrale app heeft "101" gestuurd, wat betekent dat de verwarming aan moet
            case LIGHTING_UIT:
                digitalWrite(PIN_LED_G, LOW);
                break;
            case LIGHT_REQUEST:
                Serial.write((analogRead(PIN_LDR)) *100L / (1023));
                break;
            // Er was geen commando gestuurd
            case -1:
            default:
                break;
        }
    }

    // Als de playback gepauzeerd is of als er geen muzieknummer is aangewezen.
    if (isPaused || !song_length || !song_notes || !song_durations) {
        noTone(PIN_PIEZO);
        return;
    }

    // Speel de volgende toon af als de tijd van de huidige toon verstreken is.
    if (currentMillis > millisLastNote + (1000.0 / song_durations[currentNote] * 1.2)) {
        millisLastNote = currentMillis;

        // Stop de huidige toon
        noTone(PIN_PIEZO);

        // Houd even pauze om het verschil tussen de tonen beter te horen
        delay(1000.0 / song_durations[currentNote] * 0.1);

        // Start de nieuwe toon
        tone(PIN_PIEZO, song_notes[currentNote]);

        // Ga naar de volgende toon die afgespeeld moet worden
        currentNote++;
        // Als het nummer afgelopen is (aka; er is geen volgende toon)
        if (currentNote >= song_length) {
            currentNote = 0;
        }
    }
}

/**
 * Laat een nummer zien op het 7-segment display.<br />
 * <br />
 * Eigenlijk is het 8-segment display want er is ook nog een punt.
 *
 * @param nummer Nummer tussen de 0-9
 */
static void displayNumber(uint8_t nummer) {
    switch (nummer) {
        case 0:
            // Zet alles aan behalve de middelste
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_G, LOW);
            break;

        case 1:
            // Zet alles uit behalve B en C
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], LOW);
            }
            digitalWrite(PIN_SEG_B, HIGH);
            digitalWrite(PIN_SEG_C, HIGH);
            break;

        case 2:
            // Zet alles aan behalve F en C
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_F, LOW);
            digitalWrite(PIN_SEG_C, LOW);
            break;

        case 3:
            // Zet alles aan behalve F en E
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_F, LOW);
            digitalWrite(PIN_SEG_E, LOW);
            break;

        case 4:
            // Zet alles aan behalve A, E en D
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_A, LOW);
            digitalWrite(PIN_SEG_E, LOW);
            digitalWrite(PIN_SEG_D, LOW);
            break;

        case 5:
            // Zet alles aan behalve B en E
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_B, LOW);
            digitalWrite(PIN_SEG_E, LOW);
            break;

        default: // print 'E' van Error
            // Zet alles aan behalve B en C
            for (int i = 0; i < SEG_PINS_LEN; ++i) {
                digitalWrite(SEG_PINS[i], HIGH);
            }
            digitalWrite(PIN_SEG_B, LOW);
            digitalWrite(PIN_SEG_C, LOW);
            break;
    }
}

/**
 * Wacht tot er een byte beschikbaar is op de serial en lees de byte.
 *
 * @return uint8_t 8-bit unsigned int met de data van de byte die gelezen werd
 */
static uint8_t serialWaitAndRead() {
    while (!Serial.available());

    return Serial.read();
}
