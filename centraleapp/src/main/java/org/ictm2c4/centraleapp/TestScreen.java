package org.ictm2c4.centraleapp;

import org.ictm2c4.centraleapp.arduino.MusicArduino;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.ictm2c4.centraleapp.MusicNote.*;

//@Component
public class TestScreen extends JFrame {

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    private final MusicArduino arduino;
    private JButton verwarmingAanButton = new JButton("Verwarming aan"),
                    verwarmingUitButton = new JButton("Verwarming uit"),
                    musicPauseButton    = new JButton("Pauzeer muziek"),
                    musicResumeButton   = new JButton("Hervat muziek"),
                    musicSendButton     = new JButton("Stuur testmuziek");

    private JLabel sliderLabel = new JLabel("7-segmentdisplay:");
    private JSlider slider = new JSlider(0, 9);

    {
        // Initialiseer de Arduino seriele communicatie
        this.arduino = new MusicArduino(this);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                applicationContext.close();
            }
        });
    }

    public TestScreen() {
        // Stuur een signaal naar de arduino als op de "aan" knop wordt gedrukt
        verwarmingAanButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                arduino.sendByte((byte) 101);
            }
        });

        // Stuur een signaal naar de arduino als op de "uit" knop wordt gedrukt
        verwarmingUitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                arduino.sendByte((byte) 102);
            }
        });

        // Stuur een signaal naar de arduino als er op de "pauzeer" knop wordt gedrukt
        musicPauseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                arduino.sendByte((byte) 104);
            }
        });

        // Stuur een signaal naar de arduino als er op de "hervat" knop wordt gedrukt
        musicResumeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                arduino.sendByte((byte) 105);
            }
        });

        musicSendButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MusicNote[] toonhoogtes = new MusicNote[] {B5, D5, B5, C5, C5};
                byte[]      toonlengtes = new byte[]      {2,  2,  2,  4,  4 };

                arduino.sendMusic(toonhoogtes, toonlengtes);
            }
        });

        // Stuur een signaal en de value van de slider wanneer de slider wordt bewogen
        slider.addChangeListener(new ChangeListener() {
            private int lastValue;

            @Override
            public void stateChanged(ChangeEvent e) {
                if (!slider.getValueIsAdjusting()) {
                    int value = slider.getValue();

                    if (value != lastValue) {
                        lastValue = value;

                        arduino.sendByte((byte) 103);
                        arduino.sendByte((byte) value);
                    }
                }
            }
        });

        // Zet de buttons op het scherm
        this.setLayout(new GridLayout(4, 2));
        this.add(verwarmingAanButton);
        this.add(verwarmingUitButton);
        this.add(sliderLabel);
        this.add(slider);
        this.add(musicPauseButton);
        this.add(musicResumeButton);
        this.add(musicSendButton);

        // Trigger initial buttons
        verwarmingAanButton.doClick();
        verwarmingUitButton.doClick();
        slider.setValue(1);

        // Detecteer en set de window size
        this.pack();
        // Plaats het window in het midden van het scherm
        this.setLocationRelativeTo(null);
        this.setTitle("Arduino Testscherm");
    }
}
