package org.ictm2c4.centraleapp.profile.dialog;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.OptionPaneUI;
import java.awt.*;

@Component
public class ProfileCenterPanel extends JPanel implements ChangeListener{
    private JLabel name, temperature, light, tSlider, lSlider;
    private JTextField nameText;
    private JSlider temperatureSlider, lightSlider;
    private Font paragraph = new Font("Arial", Font.PLAIN, 12);

    @PostConstruct
    public void init(){
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        setBorder(BorderFactory.createEmptyBorder(20, 50, 20, 50));
        c.ipadx = 5;
        c.ipady = 15;

        c.gridx = 0;
        c.gridy = 0;
        name = new JLabel("Naam");
        name.setFont(paragraph);
        add(name, c);

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 2;
        nameText = new JTextField(10);

        System.out.println(nameText.getText());
        add(nameText, c);

        c.gridx = 0;
        c.gridy = 1;
        temperature = new JLabel("Temperatuur");
        temperature.setFont(paragraph);
        add(temperature, c);

        c.gridx = 1;
        c.gridy = 1;
        temperatureSlider = new JSlider(12, 35, 18);
        temperatureSlider.addChangeListener(this);
        add(temperatureSlider, c);


        c.gridx = 2;
        c.gridy = 1;
        tSlider = new JLabel(String.valueOf(temperatureSlider.getValue()));
        add(tSlider, c);

        c.gridx = 0;
        c.gridy = 2;
        light = new JLabel("Lichtsterkte");
        light.setFont(paragraph);
        add(light, c);

        c.gridx = 1;
        c.gridy = 2;
        lightSlider = new JSlider(0, 100, 25);
        lightSlider.addChangeListener(this);
        add(lightSlider, c);

        c.gridx = 2;
        c.gridy = 2;
        lSlider = new JLabel(String.valueOf(lightSlider.getValue()));
        add(lSlider, c);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == temperatureSlider){
            tSlider.setText(String.valueOf(temperatureSlider.getValue()));
        }
        else if(e.getSource() == lightSlider){
            lSlider.setText(String.valueOf(lightSlider.getValue()));
        }
    }

    public boolean nameOk(){
        if (nameText.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Gebruikersnaam is leeg");
            return false;
        }
        else{
            return true;
        }
    }

    public String getTextfieldName(){
        return nameText.getText();
    }

    public int getTempSlider(){
        return temperatureSlider.getValue();
    }

    public int getLightSlider(){
        return lightSlider.getValue();
    }
}
