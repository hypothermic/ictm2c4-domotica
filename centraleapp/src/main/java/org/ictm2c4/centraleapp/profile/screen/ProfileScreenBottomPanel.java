package org.ictm2c4.centraleapp.profile.screen;

import org.ictm2c4.centraleapp.profile.dialog.ProfileDialog;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class ProfileScreenBottomPanel extends JPanel implements ActionListener {

    @InjectMocks
    @Autowired
    private ProfileScreen parentScreen;

    @InjectMocks
    @Autowired
    private ApplicationContext applicationContext;

    private CustomButton addProfile;

    @PostConstruct
    public void init() {
        addProfile = new CustomButton("Create profile");
        addProfile.addActionListener(this);
        add(addProfile);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addProfile) {
            parentScreen.setVisible(false);
            applicationContext.getBean(ProfileDialog.class).setVisible(true);
        }
    }
}
