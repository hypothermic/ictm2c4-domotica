package org.ictm2c4.centraleapp.profile.dialog;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;

@Component
public class ProfileTopPanel extends JPanel{
    private JLabel createProfile;

    @PostConstruct
    public void init(){
        setLayout(new FlowLayout());
        createProfile = new JLabel("Create profile");
        createProfile.setFont(new Font("Arial", Font.BOLD, 30));
        add(createProfile);
    }
}
