package org.ictm2c4.centraleapp.profile.dialog;

import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreen;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class ProfileBottomPanel extends JPanel{
    private CustomButton save;

    @PostConstruct
    public void init(){
        setLayout(new FlowLayout());

        save = new CustomButton("Save profile");
        add(save);
    }

    public void addConfirmListener(ActionListener listener) {
        save.addActionListener(listener);
    }
}
