package org.ictm2c4.centraleapp.profile.screen;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;

@Component
public class ProfileScreenTopPanel extends JPanel {

    private JLabel chooseProfile;

    @PostConstruct
    public void init() {
        setLayout(new FlowLayout());

        chooseProfile = new JLabel("Profiles");
        chooseProfile.setFont(new Font("Arial", Font.BOLD, 26));
        add(chooseProfile);
    }
}
