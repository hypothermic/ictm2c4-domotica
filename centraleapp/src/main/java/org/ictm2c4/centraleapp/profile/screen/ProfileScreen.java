package org.ictm2c4.centraleapp.profile.screen;

import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.profile.dialog.ProfileDialog;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
@Scope("singleton")
@Configuration
public class ProfileScreen extends JFrame {

    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private ProfileScreenProfilesPanel profileScreenProfilesPanel;

    @Autowired
    private ProfileScreenTopPanel topPanel;

    @Autowired
    private ProfileScreenBottomPanel bottomPanel;

    @PostConstruct
    public void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new BorderLayout(30, 50));

        // Toppanel is created
        add(topPanel, BorderLayout.NORTH);

        // The center panel with the profiles is created
        add(profileScreenProfilesPanel, BorderLayout.CENTER);

        // The bottom panel with the create profile button is created
        add(bottomPanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
    }

    public void refresh() {
        profileScreenProfilesPanel.refreshProfiles();
    }
}
