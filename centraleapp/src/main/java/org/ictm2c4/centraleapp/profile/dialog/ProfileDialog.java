package org.ictm2c4.centraleapp.profile.dialog;

import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreen;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.util.List;

@Component
public class ProfileDialog extends JDialog {

    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @InjectMocks
    @Autowired
    private ProfileScreen profileScreen;

    @InjectMocks
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ProfileTopPanel topPanel;

    @Autowired
    private ProfileCenterPanel profileCenterPanel;

    @Autowired
    private ProfileBottomPanel profileBottomPanel;

    private Profile profile;
    private List<Profile> profiles;

    @PostConstruct
    public void init() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout(40, 40));

        add(topPanel, BorderLayout.NORTH);
        add(profileCenterPanel, BorderLayout.CENTER);
        add(profileBottomPanel, BorderLayout.SOUTH);

        profileBottomPanel.addConfirmListener(e -> {
            if (profileCenterPanel.nameOk()) {
                profiles = profileDao.getAllProfiles();
                int id = profiles.size() + 9900;

                profile = new Profile();
                profile.setId(id);
                profile.setNaam(profileCenterPanel.getTextfieldName());
                profile.setMinLight(profileCenterPanel.getLightSlider());
                profile.setMinTemp(profileCenterPanel.getTempSlider());
                profileDao.createProfile(profile);
                Tools.writeLog("Profiel '" + profile.getNaam() + "' aangemaakt");

                setVisible(false);
                dispose();

                profileScreen.refresh();
                profileScreen.setVisible(true);
            }
        });

        pack();
        setLocationRelativeTo(null);
    }
}
