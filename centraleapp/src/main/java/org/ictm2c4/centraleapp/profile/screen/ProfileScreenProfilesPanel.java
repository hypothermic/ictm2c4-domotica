package org.ictm2c4.centraleapp.profile.screen;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Component
@Scope("singleton")
public class ProfileScreenProfilesPanel extends JPanel {

    @InjectMocks
    @Autowired
    private ProfileScreen parentScreen;

    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @InjectMocks
    @Autowired
    private ApplicationContext applicationContext;

    private List<Profile> profiles;
    private Collection<CustomButton> profileButtons = new HashSet<>();

    private volatile int lastChosenProfile;

    @PostConstruct
    public void init() {
        setLayout(new FlowLayout());
        lastChosenProfile = 69;

        setVisible(true);
        setMinimumSize(new Dimension(500, 300));

        // Het is beter om I/O operations op een aparte thread te doen,
        // maar om het simpel te houden doen we het gewoon op de UI Thread nadat de windows geladen zijn.
        EventQueue.invokeLater(this::refreshProfiles);
    }

    public void refreshProfiles() {
        profiles = profileDao.getAllProfiles();

        // Als er al buttons waren geladen, verwijder deze vanaf het panel.
        profileButtons.forEach(this::remove);
        profileButtons.clear();

        // Maak voor elk profiel een nieuwe button aan
        for (Profile profile : profiles) {
            CustomButton profileButton = new CustomButton("Profile button", 30, 30, profile.getNaam());

            profileButton.addActionListener(e -> {
                lastChosenProfile = profile.getId();

                parentScreen.setVisible(false);
                Tools.writeLog("Profiel '" + profile.getNaam() + "' gekozen");
                applicationContext.getBean(MainScreen.class).setVisible(true);
            });

            profileButtons.add(profileButton);
            add(profileButton);
        }

        // Check of alle buttons op het ProfileScreen passen (en zo niet, maak het frame groter)
        parentScreen.pack();
    }

    @Bean
    @Scope("prototype")
    public int getActiveProfile() {
        return lastChosenProfile;
    }
}
