package org.ictm2c4.centraleapp.persistance;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.PlaylistLine;
import org.ictm2c4.centraleapp.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * CRUD data access object voor de AfspeellijstRegel tabel.
 */
@Repository
@EnableTransactionManagement
@Transactional
public class PlaylistLineDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Zoek een playlist line op <i>id</i>.
     */
    public PlaylistLine findPlaylistLine(int id) {
        return sessionFactory.getCurrentSession().find(PlaylistLine.class, id);
    }

    /**
     * Sla een nieuwe <i>playlistLine</i> op.
     */
    public void createPlaylistLine(PlaylistLine playlistLine) {
        sessionFactory.getCurrentSession().save(playlistLine);
    }

    /**
     * Delete een playlist op <i>id</i>
     */
    public void deletePlaylistLine(int id) {
        sessionFactory.getCurrentSession().delete(findPlaylistLine(id));
    }

    /**
     * Voeg een lijst met <i>tracks</i> toe aan de playlist met <i>id</i>.
     *
     * @param id ID van de destination playlist.
     * @param tracks Een Collection met tracks welke toegevoegd moeten worden.
     */
    public void addTracks(int id, Collection<Track> tracks) {
        for (Track track : tracks) {
            createPlaylistLine(new PlaylistLine(id, track.getId()));
        }
    }

    /**
     * Verwijder alle tracks in de playlist met als ID <i>playlistId</i>.
     */
    public void removeAllTracks(int playlistId) {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Track> q = sessionFactory.getCurrentSession()
                .createQuery("delete from PlaylistLine where playlistId = " + playlistId);
        q.executeUpdate();
    }
}