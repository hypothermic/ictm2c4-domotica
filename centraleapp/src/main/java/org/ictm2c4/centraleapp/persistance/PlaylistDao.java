package org.ictm2c4.centraleapp.persistance;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * CRUD data access object voor de Afspeellijst tabel.
 */
@Repository
@EnableTransactionManagement
@Transactional
public class PlaylistDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Zoek een playlist op <i>id</i>.
     */
    public Playlist findPlaylist(int id) {
        return sessionFactory.getCurrentSession().find(Playlist.class, id);
    }

    /**
     * Sla een nieuwe <i>playlist</i> op.
     */
    public void createPlaylist(Playlist playlist) {
        sessionFactory.getCurrentSession().save(playlist);
    }

    /**
     * Delete een playlist op <i>id</i>
     */
    public void deletePlaylist(int id) {
        sessionFactory.getCurrentSession().delete(findPlaylist(id));
    }

    /**
     * Verkrijg het aantal tracks in een playlist.
     */
    public long getNumberOfTracks(int playlistId) {
        return (long) sessionFactory.getCurrentSession().
                createQuery("select count(*) from PlaylistLine where playlistId = " + playlistId).list().get(0);
    }

    /**
     * Gets all tracks from a playlist starting from a certain offset and within a certain limit
     *
     * @param playlistId the id of the playlist
     * @param offset the starting position
     * @param limit the amount of tracks to retrieve
     * @return returns a list of tracks
     */
    public List<Track> getAllTracks(int playlistId, int offset, int limit) {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Track> query = sessionFactory.getCurrentSession()
                .createQuery("from Track where id in (select trackId from PlaylistLine where playlistId = " + playlistId + ")")
                .setFirstResult(offset) // set the start position
                .setMaxResults(limit); // set the max number of results
        return query.list();
    }

    /**
     * Verkrijg alle tracks in een playlist
     */
    public List<Track> getAllTracks(int playlistId) {
        return getAllTracks(playlistId, 0, Integer.MAX_VALUE);
    }
}