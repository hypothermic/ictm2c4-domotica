package org.ictm2c4.centraleapp.persistance;

import lombok.extern.apachecommons.CommonsLog;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * CRUD data access object voor de Profiel tabel.
 */
@Scope("singleton")
@Repository
@EnableTransactionManagement
@Transactional
@CommonsLog
public class ProfileDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Zoek een profile op <i>id</i>.
     */
    public Profile findProfile(int id) {
        sessionFactory.getCache().evictAll();
        return sessionFactory.getCurrentSession().find(Profile.class, id);
    }

    /**
     * Sla een nieuwe <i>profile</i> op.
     */
    public void createProfile(Profile profile) {
        sessionFactory.getCurrentSession().save(profile);
    }

    /**
     * Delete een profile op <i>id</i>
     */
    public void deleteProfile(int id) {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Track> removePlaylistLines = sessionFactory.getCurrentSession()
                .createQuery("delete from PlaylistLine where playlistId in (select id from Playlist where profileId = " + id + ")");
        removePlaylistLines.executeUpdate();

        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Track> removePlaylists = sessionFactory.getCurrentSession()
                .createQuery("delete from Playlist where profileId = " + id);
        removePlaylists.executeUpdate();

        sessionFactory.getCurrentSession().delete(findProfile(id));
    }

    /**
     * Verkrijg alle profiles.
     *
     * @return List met alle bestaande profiles.
     */
    public List<Profile> getAllProfiles() {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Profile> query = sessionFactory.getCurrentSession().createQuery("from Profile");
        return query.list();
    }

    /**
     * Verkrijg het aantal playlists die behoren tot het profiel met <i>profileId</i>
     */
    public long getNumberOfPlaylists(int profileId) {
        return (long) sessionFactory.getCurrentSession().
                createQuery("select count(*) from Playlist where profileId = " + profileId).list().get(0);
    }

    public List<Playlist> getAllPlaylists(int profileId, int offset, int limit) {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Playlist> query = sessionFactory.getCurrentSession().createQuery("from Playlist where profileId = " + profileId)
                .setFirstResult(offset) // set the start position
                .setMaxResults(limit); // set the max number of results

        return query.list();
    }

    public long getTotalNumberOfPlaylists() {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Long> query = sessionFactory.getCurrentSession().createQuery("select count(*) from Playlist");
        if (query.list().isEmpty()) {
            return 0;
        }
        return query.list().get(0);
    }

    public void setTemperature(Profile profile, int temperature) {
        Profile profile1 = findProfile(profile.getId());
        profile1.setMinTemp(temperature);
        Tools.writeLog("Setting preferred temperature " + profile1.getMinTemp());
        sessionFactory.getCurrentSession().saveOrUpdate(profile1);
    }

    public void setLight(Profile profile, int light) {
        Profile profile1 = findProfile(profile.getId());
        profile1.setMinLight(light);
        Tools.writeLog("Setting preferred light " + profile1.getMinLight());
        sessionFactory.getCurrentSession().saveOrUpdate(profile1);
    }
}
