package org.ictm2c4.centraleapp.persistance;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * CRUD data access object voor de Track tabel.
 */
@Repository
@EnableTransactionManagement
@Transactional
public class TrackDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Zoek een Track op <i>id</i>.
     */
    public Track findTrack(int id) {
        return sessionFactory.getCurrentSession().find(Track.class, id);
    }

    /**
     * Sla een nieuwe <i>track</i> op.
     */
    public void createTrack(Track track) {
        sessionFactory.getCurrentSession().save(track);
    }

    /**
     * Delete een track op <i>id</i>
     */
    public void deleteTrack(int id) {
        sessionFactory.getCurrentSession().delete(findTrack(id));
    }

    /**
     * Verkrijg het aantal van tracks in de database.
     */
    public long getTotalNumberOfTracks() {
        return (long) sessionFactory.getCurrentSession().
                createQuery("select count(*) from Track").list().get(0);
    }

    /**
     * Verkrijg alle tracks in de database met pagination support.
     *
     * @param offset De startpositie
     * @param limit Het aantal tracks om op te vragen
     */
    public List<Track> getAllTracks(int offset, int limit) {
        @SuppressWarnings("unchecked") // prevent unnecessary warning
                Query<Track> query = sessionFactory.getCurrentSession().createQuery("from Track")
                .setFirstResult(offset) // set the start position
                .setMaxResults(limit); // set the max number of results
        return query.list();
    }
}
