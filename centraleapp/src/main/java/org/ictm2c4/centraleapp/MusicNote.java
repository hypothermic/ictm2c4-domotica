package org.ictm2c4.centraleapp;

/**
 * De toonhoogtes van alle muzieknoten die op de muziekspeler afgespeeld kunnen worden.<br />
 * <br />
 * De frequentie van de piezospeker is hieraan gelinkt.
 */
public enum MusicNote {

    C4 (262),
    D4 (294),
    E4 (330),
    F4 (349),
    G4 (392),
    A4 (440),
    AS4(466),
    B4 (494),
    C5 (523),
    CS5(554),
    D5 (587),
    DS5(622),
    E5 (659),
    F5 (698),
    FS5(740),
    G5 (784),
    GS5(831),
    A5 (880),
    AS5(932),
    B5 (988),

    ;

    public static MusicNote fromString(String note) {
        int frequency = Integer.parseInt(note);

        for (MusicNote value : values()) {
            if (value.frequency == frequency) {
                return value;
            }
        }

        throw new IllegalArgumentException("No note found for input " + note);
    }

    private final int frequency;

    MusicNote(int frequency) {
        this.frequency = frequency;
    }

    public int getFrequency() {
        return frequency;
    }
}
