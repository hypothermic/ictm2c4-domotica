package org.ictm2c4.centraleapp.scheduler;

import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.MainScreen.TopPanel.RPiInfoPanel;
import org.ictm2c4.centraleapp.arduino.MusicArduino;
import org.ictm2c4.centraleapp.model.LuchtvochtigheidMeting;
import org.ictm2c4.centraleapp.model.TemperatuurMeting;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.PrematureCloseException;

import javax.swing.*;

@CommonsLog
public class OmgevingsdataUpdateJob implements Job {

    public static final String CONTEXT_PANEL_OBJECT_KEY = "RPiInfoPanel",
                               PROFILE_ID_OBJECT_KEY    = "ProfileId",
                               RASPBERRY_PI_ADDRESS     = "http://192.168.0.57:8080";

    public static final int    TEMPERATUUR_CORRECTIE = -10;

    @Override
    @SneakyThrows
    public void execute(JobExecutionContext context) {
        RPiInfoPanel rpiInfoPanel = (RPiInfoPanel) context.getScheduler().getContext().get(CONTEXT_PANEL_OBJECT_KEY);
        MainScreen mainScreen = rpiInfoPanel.getMainScreen();

        if (mainScreen == null) {
            return;
        }

        MusicArduino arduino = mainScreen.getArduino();

        try {
            LuchtvochtigheidMeting luchtvochtigheidMeting = WebClient.create(RASPBERRY_PI_ADDRESS)
                    .get()
                    .uri("/v1/luchtvochtigheid")
                    .accept(MediaType.APPLICATION_XML)
                    .exchange()
                    .block()
                    .bodyToMono(LuchtvochtigheidMeting.class)
                    .block();

            rpiInfoPanel.update(luchtvochtigheidMeting);

            TemperatuurMeting temperatuurMeting = WebClient.create(RASPBERRY_PI_ADDRESS)
                    .get()
                    .uri("/v1/temperatuur")
                    .accept(MediaType.APPLICATION_XML)
                    .exchange()
                    .block()
                    .bodyToMono(TemperatuurMeting.class)
                    .block();

            temperatuurMeting.setTemperatuur(temperatuurMeting.getTemperatuur() + TEMPERATUUR_CORRECTIE);
            rpiInfoPanel.update(temperatuurMeting);

            int profileId = (int) context.getScheduler().getContext().get(PROFILE_ID_OBJECT_KEY);

            // Als temperatuur lager is dan minimumtemperatuur in profiel
            arduino.setVerwarmingEnabled(temperatuurMeting.getTemperatuur() < mainScreen.getProfileDao().findProfile(profileId).getMinTemp());

        } catch (Exception e) {
            // PrematureCloseException is normaal.
            if (!(e.getCause() instanceof PrematureCloseException)) {
                JOptionPane.showMessageDialog(null, "Raspberry Pi Sensor Host niet bereikbaar.");
            }
        }
    }
}
