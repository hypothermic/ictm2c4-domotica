package org.ictm2c4.centraleapp.scheduler;

import lombok.extern.apachecommons.CommonsLog;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@CommonsLog
@Configuration
public class OmgevingsdataUpdateJobConfig {

    /**
     * Update standaard de gegevens elke 3 seconden.
     */
    private static final int DEFAULT_UPDATE_RATE = 3000; // ms

    @Autowired
    private Environment environment;

    /**
     * Maak een nieuwe JobDetail aan die aan de trigger kan worden meegegeven
     */
    @Bean
    public JobDetail omgevingsDataUpdateJobDetail() {
        return newJob()
                .ofType(OmgevingsdataUpdateJob.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("RPi_Update_Job_Detail"))
                .withDescription("Update de gegevens van de Raspberry Pi.")
                .build();
    }

    /**
     * Maak een nieuwe trigger aan met het ingestelde tijdsinterval.
     */
    @Bean
    @Qualifier("omgevingsDataUpdateTrigger")
    public Trigger omgevingsDataUpdateTrigger(@Qualifier("omgevingsDataUpdateJobDetail") JobDetail job) {
        return newTrigger()
                .forJob(job)
                .withIdentity(TriggerKey.triggerKey("RPi_Update_Trigger"))
                .withDescription("Raspberry Pi gegevens ophaal job trigger")
                .withSchedule(simpleSchedule()
                        .withIntervalInMilliseconds(environment.getProperty("ca.scheduler.rpi.update.rate", int.class, DEFAULT_UPDATE_RATE))
                        .withMisfireHandlingInstructionFireNow()
                        .repeatForever())
                .build();
    }
}
