package org.ictm2c4.centraleapp.scheduler;

import lombok.extern.apachecommons.CommonsLog;
import org.mockito.InjectMocks;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@CommonsLog
@Configuration
public class QuartzSchedulerConfig {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AutoWiringSpringBeanJobFactory jobFactory;

    /**
     * Verkrijg een nieuwe scheduler van een SchedulerFactory
     */
    @Bean
    @Scope("singleton")
    public Scheduler scheduler(@Qualifier("schedulerFactoryBean") SchedulerFactoryBean factory) {
        return factory.getScheduler();
    }

    /**
     * Maak een nieuwe SchedulerFactory aan met onze quartz properties file
     */
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();

        jobFactory.setApplicationContext(applicationContext);
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();

        factory.setJobFactory(jobFactory);
        factory.setQuartzProperties(Objects.requireNonNull(propertiesFactoryBean.getObject()));
        return factory;
    }
}