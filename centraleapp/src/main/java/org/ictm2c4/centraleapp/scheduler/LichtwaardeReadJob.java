package org.ictm2c4.centraleapp.scheduler;

import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.MainScreen.TopPanel.RPiInfoPanel;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.arduino.MusicArduino;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

@CommonsLog
public class LichtwaardeReadJob implements Job {

    public static final String CONTEXT_PANEL_OBJECT_KEY = "RPiInfoPanel",
                               PROFILE_ID_OBJECT_KEY    = "ProfileId";

    @Override
    @SneakyThrows
    public void execute(JobExecutionContext context) {
        RPiInfoPanel rpiInfoPanel = (RPiInfoPanel) context.getScheduler().getContext().get(CONTEXT_PANEL_OBJECT_KEY);
        MainScreen mainScreen = rpiInfoPanel.getMainScreen();

        if (mainScreen == null) {
            return;
        }

        MusicArduino arduino = mainScreen.getArduino();

        // Stuur LIGHT_REQUEST commando, zie de documentatie in main.cpp hierover voor meer uitleg.
        arduino.sendByte(MusicArduino.COMMAND_LIGHT_REQUEST);

        // Wacht tot we de data binnen hebben
        //noinspection StatementWithEmptyBody
        while (arduino.getAvailableBytesAmount() < 1);

        // Lees de waarde
        byte lichtIntensiteit = arduino.readByte();

        int profileId = (int) context.getScheduler().getContext().get(PROFILE_ID_OBJECT_KEY);
        boolean lightingEnabled = lichtIntensiteit < mainScreen.getProfileDao().findProfile(profileId).getMinLight();

        // Lees de data en update het Licht intensiteit label.
        rpiInfoPanel.getLichtintensiteitLabel().setText("Lichtintensiteit: " + lichtIntensiteit + "%");

        // Als lichtintensiteit lager is dan minimumwaarde in profiel
        arduino.setVerlichtingEnabled(lightingEnabled);

        Tools.writeLog("Gemeten lichtintensiteit: " + lichtIntensiteit);
        Tools.writeLog("Verlichting " + (lightingEnabled ? "ingeschakeld" : "uitgeschakeld"));
    }
}
