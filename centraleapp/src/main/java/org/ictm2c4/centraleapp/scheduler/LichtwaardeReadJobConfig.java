package org.ictm2c4.centraleapp.scheduler;

import lombok.extern.apachecommons.CommonsLog;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@CommonsLog
@Configuration
public class LichtwaardeReadJobConfig {

    /**
     * Update standaard de gegevens elke seconde.
     */
    private static final int DEFAULT_UPDATE_RATE = 1000; // ms

    @Autowired
    private Environment environment;

    /**
     * Maak een nieuwe JobDetail aan die aan de trigger kan worden meegegeven
     */
    @Bean
    public JobDetail lichtwaardeReadJobDetail() {
        return newJob()
                .ofType(LichtwaardeReadJob.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("Arduino_Update_Job_Detail"))
                .withDescription("Update de gegevens van de Arduino.")
                .build();
    }

    /**
     * Maak een nieuwe trigger aan met het ingestelde tijdsinterval.
     */
    @Bean
    @Qualifier("lichtwaardeReadTrigger")
    public Trigger lichtwaardeReadTrigger(@Qualifier("lichtwaardeReadJobDetail") JobDetail job) {
        return newTrigger()
                .forJob(job)
                .withIdentity(TriggerKey.triggerKey("Arduino_Update_Trigger"))
                .withDescription("Arduino lichtwaarde ophaal job trigger")
                .withSchedule(simpleSchedule()
                        .withIntervalInMilliseconds(environment.getProperty("ca.scheduler.arduino.update.rate", int.class, DEFAULT_UPDATE_RATE))
                        .withMisfireHandlingInstructionFireNow()
                        .repeatForever())
                .build();
    }
}
