package org.ictm2c4.centraleapp.ui;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;

import javax.swing.*;

public class TemperatuurSettingPanel extends SettingPanel {

    public TemperatuurSettingPanel(ProfileDao profileDao, int minSlider, int maxSlider) {
        super(profileDao, minSlider, maxSlider);

        // Adjusts the labels en background to match the main theme
        setBackground(MainScreen.primaryBackground);
        settingLabel.setText("Minimumtemperatuur voor verwarming");
        settingLabel.setBackground(MainScreen.primaryBackground);

        // Sets the background for the slider to match the main theme
        settingSlider.setBackground(MainScreen.primaryBackground);

        // Sets the value of the temperature slider retrieved from the database
        settingSlider.setValue(profile.getMinTemp());

        // When slider is changed by the user
        settingSlider.addChangeListener(e -> {
            // Sets the label for the setting value to the current value of the slider
            settingValue.setText(settingSlider.getValue() + "°C");

            // If the slider doesn't move and doesn't match the current database value
            // Update the database value to the value on the slider
            if (!settingSlider.getValueIsAdjusting() && settingSlider.getValue() != profile.getMinTemp()) {
                profileDao.setTemperature(profile, settingSlider.getValue());
            }
        });

        try {
            settingValue.setText(profile.getMinTemp() + "°C");
        } catch (Exception e) {
            settingValue.setText("Settings kunnen niet worden geladen");
            Tools.writeLog("Failed to load settings");
        }
    }
}
