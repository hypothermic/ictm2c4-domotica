package org.ictm2c4.centraleapp.ui;

import javax.swing.*;
import java.awt.*;

public class CustomButton extends JButton {
    public static Color buttonColor = new Color(110, 115, 196);

    private JLabel iconLabel;
    private int iconWidth, iconHeight;
    private String text = null;

    // with icon & with text and a custom background color
    public CustomButton(String iconName, int iconWidth, int iconHeight, String text, Color background) {
        // create JButton
        super();
        super.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        // make sure the components don't clump together in the center
        constraints.weightx = 1;

        // add icon
        constraints.gridx = 0;
        constraints.insets = new Insets(0, 0, 0, 5);
        iconLabel = new JLabel(getResizedIcon(iconName, iconWidth, iconHeight));
        super.add(iconLabel, constraints);

        // add text
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.gridx = 1;
        super.add(new JLabel(text), constraints);

        super.setBackground(background);
        super.setFocusPainted(false);

        this.iconWidth = iconWidth;
        this.iconHeight = iconHeight;
        this.text = text;
    }

    // with icon & with text
    public CustomButton(String iconName, int iconWidth, int iconHeight, String text) {
        this(iconName, iconWidth, iconHeight, text, buttonColor);
    }

    // without icon & with text
    public CustomButton(String text, Color background) {
        // create JButton
        super(text);

        super.setBackground(background);
        super.setFocusPainted(false);

        this.text = text;
    }

    // without icon & text
    public CustomButton(String text) {
        this(text, buttonColor);
    }

    // with icon & without text
    public CustomButton(String iconName, int iconWidth, int iconHeight, Color background) {
        // create JButton
        super();
        // add icon
        iconLabel = new JLabel(getResizedIcon(iconName, iconWidth, iconHeight));
        super.add(iconLabel);

        super.setBackground(background);
        super.setFocusPainted(false);

        this.iconWidth = iconWidth;
        this.iconHeight = iconHeight;
    }

    // with icon & without text
    public CustomButton(String iconName, int iconWidth, int iconHeight) {
        this(iconName, iconWidth, iconHeight, buttonColor);
    }

    public void setIcon(String iconName) {
        this.removeAll();
        if (text != null) {
            GridBagConstraints constraints = new GridBagConstraints();
            // make sure the components don't clump together in the center
            constraints.weightx = 1;

            // add icon
            constraints.gridx = 0;
            constraints.insets = new Insets(0, 0, 0, 5);
            iconLabel = new JLabel(getResizedIcon(iconName, iconWidth, iconHeight));
            super.add(iconLabel, constraints);

            // add text
            constraints.insets = new Insets(0, 0, 0, 0);
            constraints.gridx = 1;
            super.add(new JLabel(text), constraints);
        } else {
            // add icon
            iconLabel = new JLabel(getResizedIcon(iconName, iconWidth, iconHeight));
            super.add(iconLabel);
        }
        // make new icon visible
        super.repaint();
        super.revalidate();
    }

    private static ImageIcon getResizedIcon(String iconName, int width, int height) {
        // resize icon to specified size
        ImageIcon icon = new ImageIcon("centraleapp/src/main/resources/icons/png/" + iconName);
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(newimg);
    }
}
