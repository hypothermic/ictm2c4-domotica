package org.ictm2c4.centraleapp.ui;

import java.awt.*;

// This layoutmanager is copied from https://stackoverflow.com/a/30984148/9209836
// and then slightly modified so it can also take a right component
// We need this manager for correct center alignment of components inside a JPanel
public class BottomPanelLayoutManager implements LayoutManager2 {

    public static final Integer LEFT = 0;

    public static final Integer CENTERED = 1;

    public static final Integer RIGHT = 2;

    private java.awt.Component leftComponent;

    private java.awt.Component centeredComponent;

    private java.awt.Component rightComponent;

    @Override
    public void addLayoutComponent(String name, java.awt.Component comp) {
    }

    @Override
    public void removeLayoutComponent(java.awt.Component comp) {
        if (leftComponent == comp) {
            leftComponent = null;
        } else if (centeredComponent == comp) {
            centeredComponent = null;
        } else if (rightComponent == comp) {
            rightComponent = null;
        }
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        Dimension d = new Dimension();
        for (java.awt.Component c : parent.getComponents()) {
            //wide enough to stack the left and center components horizontally without overlap
            d.width += c.getPreferredSize().width;
            //tall enough to fit the tallest component
            d.height = Math.max(d.height, c.getPreferredSize().height);
        }
        return d;
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return preferredLayoutSize(parent);
    }

    @Override
    public void layoutContainer(Container parent) {
        //in this method we will:
        //1) position the left component on the left edge of the parent and center it vertically
        //2) position the center component in the center of the parent and center it vertically
        //3) position the right component on the right edge of the parent and center it veritcally

        int leftComponentWidth = leftComponent.getPreferredSize().width;
        int leftComponentHeight = leftComponent.getPreferredSize().height;
        int rightComponentWidth = rightComponent.getPreferredSize().width;
        int rightComponentHeight = rightComponent.getPreferredSize().height;
        int centeredComponentWidth = centeredComponent.getPreferredSize().width;
        int centeredComponentHeight = centeredComponent.getPreferredSize().height;

        leftComponent.setBounds(0, (parent.getHeight() - leftComponentHeight) / 2, leftComponentWidth, leftComponentHeight);
        rightComponent.setBounds(parent.getWidth() - rightComponentWidth, (parent.getHeight() - rightComponentHeight) / 2, rightComponentWidth, rightComponentHeight);

        int centerComponentLeftEdge = (parent.getWidth() - centeredComponentWidth) / 2;
        int centerComponentTopEdge = (parent.getHeight() - centeredComponentHeight) / 2;

        centeredComponent.setBounds(centerComponentLeftEdge,
                centerComponentTopEdge,
                centeredComponentWidth,
                centeredComponentHeight);
    }

    @Override
    public void addLayoutComponent(java.awt.Component comp, Object constraints) {
        if (LEFT.equals(constraints)) {
            if (leftComponent != null) {
                throw new IllegalStateException("A left component has already been assigned to this layout.");
            }
            leftComponent = comp;
        } else if (CENTERED.equals(constraints)) {
            if (centeredComponent != null) {
                throw new IllegalStateException("A centered component has already been assigned to this layout.");
            }
            centeredComponent = comp;
        } else if (RIGHT.equals(constraints)) {
            if (rightComponent != null) {
                throw new IllegalStateException("A centered component has already been assigned to this layout.");
            }
            rightComponent = comp;
        } else {
            throw new IllegalStateException("Unexpected constraints '" + constraints + "'.");
        }
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0;
    }

    @Override
    public void invalidateLayout(Container target) {

    }
}