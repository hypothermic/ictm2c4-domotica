package org.ictm2c4.centraleapp.ui;

import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;

import javax.swing.*;
import java.awt.*;

public abstract class SettingPanel extends JPanel {

    protected JLabel settingLabel, settingValue = new JLabel("...");
    protected JSlider settingSlider;
    protected Profile profile;
    protected Font textFont, valueFont;

    public SettingPanel(ProfileDao profileDao, int minSlider, int maxSlider) {
        // Profile placeholder, maakt hier profile aan
        profile = profileDao.findProfile(69);

        // Set de layout manager van de panel met padding
        setLayout(new BorderLayout(30, 30));
        setBorder(BorderFactory.createEmptyBorder(30,30,30,30));

        // The fonts are created
        textFont = new Font(null, Font.BOLD, 16);
        valueFont = new Font(null, Font.BOLD, 24);

        // Creates the components
        settingLabel = new JLabel();
        settingSlider = new JSlider(minSlider, maxSlider);

        // Sets the font of the settingslabel
        settingLabel.setFont(textFont);

        // Adds the setting label and the slider to the panel
        add(settingLabel, BorderLayout.WEST);
        add(settingSlider, BorderLayout.SOUTH);

        // Sets the font of the settingValue
        settingValue.setFont(valueFont);
        // Adds the setting value label to the panel
        add(settingValue, BorderLayout.EAST);

    }
}
