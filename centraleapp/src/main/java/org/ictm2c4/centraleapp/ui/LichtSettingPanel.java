package org.ictm2c4.centraleapp.ui;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;

import javax.swing.*;

public class LichtSettingPanel extends SettingPanel {

    public LichtSettingPanel(ProfileDao profileDao, int minSlider, int maxSlider) {
        super(profileDao, minSlider, maxSlider);

        // Adjusts the labels en background to match the main theme
        setBackground(MainScreen.secondaryBackground);
        settingLabel.setText("Minimum licht voor muziek-playback");
        settingLabel.setBackground(MainScreen.secondaryBackground);

        // Sets the background for the slider to match the main theme
        settingSlider.setBackground(MainScreen.secondaryBackground);

        // Sets the value of the light slider, retrieved from the database
        settingSlider.setValue(profile.getMinLight());

        // When slider is changed by the user
        settingSlider.addChangeListener(e -> {
            // Sets the label for the setting value to the current value of the slider
            settingValue.setText(String.valueOf(settingSlider.getValue()));

            // If the slider doesn't move and doesn't match the current database value
            // Update the database value to the value on the slider
            if (!settingSlider.getValueIsAdjusting() && settingSlider.getValue() != profile.getMinLight()) {
                profileDao.setLight(profile, settingSlider.getValue());
            }
        });

        try {
            settingValue.setText(String.valueOf(profile.getMinLight()));
        } catch (Exception e) {
            settingValue.setText("Settings kunnen niet worden geladen");
            Tools.writeLog("Failed to load settings");
        }
    }
}
