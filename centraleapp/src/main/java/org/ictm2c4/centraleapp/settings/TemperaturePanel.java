package org.ictm2c4.centraleapp.settings;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreenProfilesPanel;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

@Component
public class TemperaturePanel extends JPanel {
    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private ProfileScreenProfilesPanel profilesPanel;

    private JLabel tempLabel, tempSliderLabel;
    private JSlider tempSlider;
    private Profile profile;

    @PostConstruct
    public void init(){
        profile = profileDao.findProfile(profilesPanel.getActiveProfile());
        setLayout(new BorderLayout(30, 30));
        setBorder(BorderFactory.createEmptyBorder(30,30,30,30));
        setBackground(MainScreen.primaryBackground);

        tempLabel = new JLabel("Minimum temperatuur waarde");
        tempLabel.setBackground(MainScreen.primaryBackground);
        add(tempLabel, BorderLayout.WEST);

        tempSlider = new JSlider(12, 35, profile.getMinTemp());
        tempSlider.setBackground(MainScreen.primaryBackground);
        add(tempSlider, BorderLayout.SOUTH);

        tempSliderLabel = new JLabel(profile.getMinTemp() + "°C");
        tempSliderLabel.setBackground(MainScreen.primaryBackground);
        add(tempSliderLabel, BorderLayout.EAST);

        tempSlider.addChangeListener(e -> {
            tempSliderLabel.setText(tempSlider.getValue() + "°C");
            if (!tempSlider.getValueIsAdjusting() && tempSlider.getValue() != profile.getMinTemp()){
                profileDao.setTemperature(profile, tempSlider.getValue());
                Tools.writeLog("Temperatuurgrens veranderd naar " + tempSlider.getValue() + "°C");
            }
        });
    }

    public void update() {
        profile = profileDao.findProfile(profilesPanel.getActiveProfile());

        tempSlider.setValue(profile.getMinTemp());
    }
}
