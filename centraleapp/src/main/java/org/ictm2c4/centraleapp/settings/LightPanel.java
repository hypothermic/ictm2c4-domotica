package org.ictm2c4.centraleapp.settings;

import lombok.extern.apachecommons.CommonsLog;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreenProfilesPanel;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@Component
@CommonsLog
public class LightPanel extends JPanel {

    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private ProfileScreenProfilesPanel profilesPanel;

    private JLabel lightLabel, lightSliderLabel;
    private JSlider lightSlider;
    private Profile profile;

    @PostConstruct
    public void init(){
        profile = profileDao.findProfile(profilesPanel.getActiveProfile());
        setLayout(new BorderLayout(30, 30));
        setBorder(BorderFactory.createEmptyBorder(30,30,30,30));
        setBackground(MainScreen.secondaryBackground);

        lightLabel = new JLabel("Minimum waarde voor verlichting");
        lightLabel.setBackground(MainScreen.secondaryBackground);
        add(lightLabel, BorderLayout.WEST);

        lightSlider = new JSlider(0, 100, profile.getMinLight());
        lightSlider.setBackground(MainScreen.secondaryBackground);
        add(lightSlider, BorderLayout.SOUTH);

        lightSliderLabel = new JLabel(String.valueOf(profile.getMinLight()));
        lightSliderLabel.setBackground(MainScreen.secondaryBackground);
        add(lightSliderLabel, BorderLayout.EAST);

        lightSlider.addChangeListener(e -> {
            lightSliderLabel.setText(String.valueOf(lightSlider.getValue()));
            if ((!lightSlider.getValueIsAdjusting()) && lightSlider.getValue() != profile.getMinLight()){
                profileDao.setLight(profile, lightSlider.getValue());
                Tools.writeLog("Lichtwaardegrens veranderd naar " + lightSlider.getValue());
            }
        });
    }

    public void update() {
        profile = profileDao.findProfile(profilesPanel.getActiveProfile());

        lightSlider.setValue(profile.getMinLight());
    }
}
