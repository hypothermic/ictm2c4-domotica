package org.ictm2c4.centraleapp.settings;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;

@Component
public class SettingsDialog extends JDialog {
    @Autowired
    private TemperaturePanel temperaturePanel;

    @Autowired
    private LightPanel lightPanel;

    @Autowired
    private DeletePanel deletePanel;

    @Autowired
    private LogPanel logPanel;

    @PostConstruct
    public void init() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
        setMinimumSize(new Dimension(300, 450));
        setLayout(new GridBagLayout());
        setBackground(MainScreen.primaryBackground);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weighty = 1;

        constraints.gridy = 0;
        add(temperaturePanel, constraints);
        constraints.gridy = 1;
        add(lightPanel, constraints);
        constraints.gridy = 2;
        add(deletePanel, constraints);
        constraints.gridy = 3;
        add(logPanel, constraints);

        pack();
        setLocationRelativeTo(null);
    }

    public SettingsDialog update() {
        lightPanel.update();
        temperaturePanel.update();

        return this;
    }
}