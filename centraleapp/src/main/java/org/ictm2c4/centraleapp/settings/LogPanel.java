package org.ictm2c4.centraleapp.settings;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class LogPanel extends JPanel implements ActionListener {

    @InjectMocks
    @Autowired
    private ApplicationContext applicationContext;

    private CustomButton viewLogs;

    @Autowired
    private LogDialog logDialog;

    @PostConstruct
    public void init() {
        setLayout(new BorderLayout(30, 30));
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        setBackground(MainScreen.primaryBackground);

        viewLogs = new CustomButton("Logs bekijken");
        viewLogs.addActionListener(this);
        add(viewLogs, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LogDialog logDialog = applicationContext.getBean(LogDialog.class);
        logDialog.refresh();
        logDialog.setVisible(true);
    }
}
