package org.ictm2c4.centraleapp.settings;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.persistance.PlaylistDao;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreen;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreenProfilesPanel;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class DeletePanel extends JPanel implements ActionListener {

    @InjectMocks
    @Autowired
    private SettingsDialog settingsDialog;

    @InjectMocks
    @Autowired
    private ProfileDao profileDao;

    @InjectMocks
    @Autowired
    private ApplicationContext applicationContext;

    private CustomButton deleteProfile;

    @Autowired
    private ProfileScreenProfilesPanel profilesPanel;

    @PostConstruct
    public void init(){
        setLayout(new BorderLayout(30, 30));
        setBorder(BorderFactory.createEmptyBorder(30,30,30,30));
        setBackground(MainScreen.primaryBackground);

        deleteProfile = new CustomButton("Delete profile");
        deleteProfile.addActionListener(this);
        add(deleteProfile, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int checkResult = JOptionPane.showConfirmDialog(this, "Weet je zeker dat je het account wil verwijderen?");
        if (checkResult == JOptionPane.YES_OPTION) {
            settingsDialog.dispose();
            applicationContext.getBean(MainScreen.class).setVisible(false);
            Tools.writeLog("Profiel '" + profilesPanel.getActiveProfile() + "' verwijdered");
            profileDao.deleteProfile(profilesPanel.getActiveProfile());

            ProfileScreen profileScreen = applicationContext.getBean(ProfileScreen.class);
            profileScreen.refresh();
            profileScreen.setVisible(true);
        }
    }
}
