package org.ictm2c4.centraleapp.settings;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

@Component
public class LogDialog extends JDialog {

    private JScrollPane scrollPane;
    private DefaultTableModel tableModel;
    private JTable logTable;


    /**
     * Creates the JDialog to hold the log table
     */
    @PostConstruct
    public void init() {
        setBackground(MainScreen.primaryBackground);
        setPreferredSize(new Dimension(500, 600));
        setMinimumSize(new Dimension(500, 600));
        setResizable(false);

        logTable = new JTable();
        tableModel = (DefaultTableModel) logTable.getModel();
        tableModel.setColumnIdentifiers(new Object[]{"Tijdcode", "Gebeurtenis"});
        scrollPane = new JScrollPane(logTable);
        add(scrollPane);
    }

    /**
     * Refreshes the JTable
     */
    public void refresh() {
        tableModel.getDataVector().removeAllElements();
        for(String[] entry : Tools.readLog()) {
            tableModel.addRow(new Object[]{entry[0], entry[1]});
        }
    }
}
