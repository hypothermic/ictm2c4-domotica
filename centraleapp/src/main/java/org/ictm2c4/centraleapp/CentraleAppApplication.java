package org.ictm2c4.centraleapp;

import org.ictm2c4.centraleapp.profile.screen.ProfileScreen;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.awt.*;

@SpringBootApplication
public class CentraleAppApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(CentraleAppApplication.class).headless(false).run(args);

        // Maak het profielscherm zichtbaar zodra de applicatie ready is.
        EventQueue.invokeLater(() -> applicationContext.getBean(ProfileScreen.class).setVisible(true));
    }

}
