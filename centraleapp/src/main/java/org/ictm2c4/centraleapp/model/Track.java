package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Een muzieknummer met de metadata en de songdata.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Track")
public class Track {

    /**
     * Numeric ID van de track.
     */
    @Id
    @Column(name = "TrackID")
    private int id;

    /**
     * Titel van de track.
     */
    @Column(name = "TrackNaam")
    private String title;

    /**
     * Naam van de artiest van de track.
     */
    @Column(name = "ArtiestNaam")
    private String artist;

    /**
     * Lengte van de track in minuten.
     */
    @Column(name = "Duration")
    private double duration;

    /**
     * Notes van de track in "A:B,A:B,A:B" formaat waarbij A de toonhoogte is en B de toonlengte is.
     */
    @Column(name = "Notes")
    private String notes;

}
