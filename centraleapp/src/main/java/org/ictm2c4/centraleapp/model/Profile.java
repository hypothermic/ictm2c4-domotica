package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Een gebruikersprofiel/account.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Profiel")
public class Profile {

    /**
     * Numeric ID van het profiel
     */
    @Id
    @Column(name = "ProfielID")
    private volatile int id;

    /**
     * De naam van het profiel
     */
    private String naam;

    /**
     * De temperatuurgrens welke ingesteld is door de gebruiker
     */
    @Column(name="Temperatuur")
    private int minTemp;

    /**
     * De lichtintensiteitsgrens welke ingesteld is door de gebruiker
     */
    @Column(name="Lichtintensiteit")
    private int minLight;

}
