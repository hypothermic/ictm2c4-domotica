package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Een data object welke een temperatuurmeting representeert.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TemperatuurMeting implements Meting {

    /**
     * De temperatuur in graden Celsius als <i>double</i>.
     */
    private double temperatuur;

    /**
     * Verkrijg de gemeten temperatuur van deze meting.
     *
     * @return temperatuur in graden Celsius als <i>double</i>
     */
    @Override
    public double getWaarde() {
        return temperatuur;
    }
}
