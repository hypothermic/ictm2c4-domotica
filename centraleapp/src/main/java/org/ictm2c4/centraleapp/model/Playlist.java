package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Een afspeellijst. Hieraan kunnen tracks gekoppeld worden door middel van AfspeellijstRegels (PlaylistLine)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Afspeellijst")
public class Playlist {

    /**
     * Numeric ID van de playlist.
     */
    @Id
    @Column(name = "AfspeellijstID")
    private int id;

    /**
     * Numeric ID van het profiel van de eigenaar.
     */
    @Column(name = "ProfielID")
    private int profileId;

    /**
     * Naam van deze afspeellijst.
     */
    @Column(name = "AfspeellijstNaam")
    private String playlistName;

    /**
     * Beschrijving van deze afspeellijst.
     */
    @Column(name = "AfspeellijstBeschrijving")
    private String playlistDescription;

}