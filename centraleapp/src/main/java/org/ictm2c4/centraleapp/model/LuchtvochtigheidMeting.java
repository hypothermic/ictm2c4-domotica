package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Een data object welke een luchtvochtigheidsmeting representeert.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LuchtvochtigheidMeting implements Meting {

    /**
     * Het percentage luchtvochtigheid als <i>double</i>.
     */
    private double luchtvochtigheid;

    /**
     * Verkrijg het gemeten luchtvochtigheidspercentage van deze meting.
     *
     * @return luchtvochtigheidpercentage als <i>double</i>
     */
    @Override
    public double getWaarde() {
        return luchtvochtigheid;
    }
}
