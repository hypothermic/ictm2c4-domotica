package org.ictm2c4.centraleapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Koppeling tussen een Afspeellijst en een Track.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AfspeellijstRegel")
public class PlaylistLine implements Serializable {

    /**
     * Numeric ID van de afspeellijst
     */
    @Id
    @Column(name = "AfspeellijstID")
    private int playlistId;

    /**
     * Numeric ID van de track
     */
    @Id
    @Column(name = "TrackID")
    private int trackId;
}
