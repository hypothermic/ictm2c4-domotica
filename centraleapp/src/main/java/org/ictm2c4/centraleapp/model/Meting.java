package org.ictm2c4.centraleapp.model;

/**
 * Het resultaat van een meting.
 */
public interface Meting {

    /**
     * De gemeten waarde.
     */
    double getWaarde();

}
