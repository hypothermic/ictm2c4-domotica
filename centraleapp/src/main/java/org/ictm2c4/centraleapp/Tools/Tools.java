package org.ictm2c4.centraleapp.Tools;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Klasse met gereedschap zoals logging, etc.
 */
public class Tools {

    /**
     * Pad waar de log file zich bevindt.
     */
    public static final String FILE_NAME = "centraleapp/src/main/resources/logs/logs.txt";

    /**
     * Formatter voor het omzetten van een datum/tijd naar een string, en andersom.
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    /**
     * Probeer race-conditions m.b.t. de log file te voorkomen d.m.v. een mutex
     *     (ofwel een semaphore zoals het in de Java STL genomend wordt.)
     */
    public static final Semaphore WRITE_SEMAPHORE = new Semaphore(1),
                                  READ_SEMAPHORE  = new Semaphore(1);

    static {
        /*
         * Maak de directories waarin de log file zich bevindt aan als ze nog niet bestaan.
         */
        //noinspection ResultOfMethodCallIgnored
        new File(FILE_NAME).getParentFile().mkdirs();
    }

    /**
     * Voeg een regel toe aan het logbestand.
     *
     * @param logLine Tekstregel om toe te voegen
     */
    public static void writeLog(String logLine) {
        WRITE_SEMAPHORE.acquireUninterruptibly();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME, true));

            writer.write(DATE_TIME_FORMATTER.format(LocalDateTime.now()) + "@" + logLine);
            writer.newLine();

            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            WRITE_SEMAPHORE.release();
        }
    }

    /**
     * Lees alle regels van het logbestand.
     *
     * @return List<String[]> Een list met daarin alle regels.
     *             Elke regel is een String[2] met twee elementen;
     *             het eerste element is een timestamp en het tweede element is de tekst
     */
    public static List<String[]> readLog() {
        ArrayList<String[]> logs = new ArrayList<>();

        READ_SEMAPHORE.acquireUninterruptibly();
        try {
            BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));
            String line;

            while ((line = br.readLine()) != null) {
                String timestamp = line.split("@")[0];
                String logLine = line.split("@")[1];

                logs.add(new String[]{timestamp, logLine});
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            READ_SEMAPHORE.release();
        }

        return logs;
    }
}
