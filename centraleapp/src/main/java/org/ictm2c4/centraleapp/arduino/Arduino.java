package org.ictm2c4.centraleapp.arduino;

import com.fazecast.jSerialComm.SerialPort;

import javax.swing.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Arduino {

    /**
     * Het object dat de seriele poort representeert.
     */
    private static volatile SerialPort port;

    /**
     * Het frame waaraan de error dialogs worden gekoppeld.
     */
    private final JFrame parentFrame;

    public Arduino(JFrame parentFrame) {
        this.parentFrame = parentFrame;
        openPort();
    }

    public Arduino() {
        // if no parent frame is given, create one
        this(new JFrame());
    }

    /**
     * Open een SerialPort verbinding en verkrijg de instance.<br />
     * <br />
     * Eigenlijk zou dit op een aparte thread uitgevoerd moeten wordenwant het
     * is een blocking IO operation, anders zal de UI zal een korte tijd vastlopen.
     */
    private void openPort() {
        if (port != null) return;

        // loop through all ports
        SerialPort[] allPorts = SerialPort.getCommPorts();

        // if no ports are found, show error and return without opening one
        if (allPorts.length == 0) {
            showError(parentFrame, "Could not find any port. \n" +
                    "Please check if the arduino had been connected");
            return;
        }

        for (SerialPort p : allPorts) {
            port = SerialPort.getCommPort(p.getSystemPortName());

            // attempt to open the port
            port.openPort(1000);
            if (!port.isOpen()) {
                showError(parentFrame, "Port not available");
            }
        }

        // default connection settings for Arduino
        port.setComPortParameters(9600, 8, 1, 0);
        // Timeout setting to make sure writing and reading is done correctly
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING,
                0, 0);
    }

    /**
     * Stuur een byte naar de Arduino en flush de output stream<br />
     * <br />
     * Let er wel op dat de Arduino two's compliment signing gebruikt,
     * dus als je de byte leest als unsigned int wordt er 128 bij "opgeteld".
     *
     * @param b De byte die gestuurd moet worden.
     */
    public void sendByte(byte b) {
        if (port == null) return;

        try {
            port.getOutputStream().write(b);
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    /**
     * Stuur enkele bytes naar de Arduino en flush de output stream.
     *
     * @param bytes Een array van bytes die gestuurd moeten worden
     */
    public void sendBytes(byte... bytes) {
        if (port == null) return;

        try {
            for (byte b : bytes) {
                port.getOutputStream().write(b);
            }
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    /**
     * Verstuur een string naar de Arduino.
     *
     * @deprecated omdat dit geen cross-platform approach is, want de Arduino kan crashen
     *             als de JRE default charset op Cyrillic staat. Het beste is om de string
     *             eerst naar utf-8 bytes om te zetten en dan te sturen via sendBytes().
     */
    @Deprecated(forRemoval = true)
    public void send(String message) {
        if (port == null) return;

        // attempt to write a string to the Arduino
        try {
            port.getOutputStream().write(message.getBytes());
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    /**
     * Check hoeveel bytes de Arduino verstuurd heeft.
     *
     * @return int Hoeveel bytes de Arduino verstuurd heeft.
     */
    public int getAvailableBytesAmount() {
        return port.bytesAvailable();
    }

    /**
     * Ontvang een byte van de Arduino.
     *
     * @return byte De ontvangen waarde
     */
    public byte readByte() {
        byte[] bytes = new byte[1];

        port.readBytes(bytes, 1);

        return bytes[0];
    }

    /**
     * Ontvang een char sequence van de Arduino.
     *
     * @deprecated omdat dit geen cross-platform approach is, want de Java runtime kan crashen
     *             als de JRE default charset op Cyrillic staat. Het beste is om de string
     *             eerst naar utf-8 bytes om te zetten en dan te sturen via sendBytes().
     */
    @Deprecated(forRemoval = true)
    public String receive() {
        if (port == null) return "No open port";

        String message = "";
        // wait at most 1 minute for data to come in
        long currentTime = System.currentTimeMillis();
        long stopTime = currentTime + 60000;
        while (System.currentTimeMillis() < stopTime) {
            try {
                // read all the incomming bytes and put them into a string
                byte[] b = port.getInputStream().readNBytes(port.bytesAvailable());
                message = new String(b, StandardCharsets.UTF_8);
                // if the message contains data, break the loop
                if (!message.equals("")) {
                    break;
                }
            } catch (IOException e) {
                showError(parentFrame, "Error while reading from the Arduino");
            }
        }

        if (!message.equals("")) {
            return message;
        } else {
            showError(parentFrame, "The Arduino took to long to respond. \n" +
                    "Please restart the application.");
            return "";
        }
    }

    /**
     * Laat een modal error dialog verschijnen die gekoppeld is aan <i>parent</i>.
     *
     * @param parent Het frame waaraan de dialog gekoppeld moet worden
     * @param errorMessage De foutmelding die moet verschijnen.
     */
    private static void showError(JFrame parent, String errorMessage) {
        JOptionPane.showMessageDialog(parent, errorMessage, "An error occured", JOptionPane.ERROR_MESSAGE);
    }
}