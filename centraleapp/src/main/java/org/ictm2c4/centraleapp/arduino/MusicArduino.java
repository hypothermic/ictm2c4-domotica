package org.ictm2c4.centraleapp.arduino;

import org.ictm2c4.centraleapp.MainScreen.BottomPanel.CenterPanel;
import org.ictm2c4.centraleapp.MusicNote;

import javax.swing.*;

/**
 * Wrapper class voor de Arduino met utility functions voor ons protocol.
 */
public class MusicArduino extends Arduino {

    /**
     * Commando's die naar de Arduino kunnen worden gestuurd om een bepaalde reactie te triggeren.<br />
     * <br />
     * Voor meer documentatie over de werking van elke functie, zie <i>main.cpp</i>
     */
    public static final byte COMMAND_VERWARMING_ACTIVATE    = 101,
                             COMMAND_VERWARMING_DEACTIVATE  = 102,
                             COMMAND_SET_NUMBER_DISPLAY     = 103,
                             COMMAND_MUSIC_PAUSE            = 104,
                             COMMAND_MUSIC_RESUME           = 105,
                             COMMAND_SEND_MUSIC_DATA        = 106,
                             COMMAND_VERLICHTING_ACTIVATE   = 107,
                             COMMAND_VERLICHTING_DEACTIVATE = 108,
                             COMMAND_LIGHT_REQUEST          = 109;

    /**
     * @param parentFrame Het frame waaraan de error dialogs worden gekoppeld.
     */
    public MusicArduino(JFrame parentFrame) {
        super(parentFrame);
    }

    /**
     * (De-)activeer het verwarmingselement.
     *
     * @param enabled Of het verwarmingselement actief moet zijn.
     */
    public void setVerwarmingEnabled(boolean enabled) {
        sendByte(enabled
                     ? COMMAND_VERWARMING_ACTIVATE
                     : COMMAND_VERWARMING_DEACTIVATE);
    }

    /**
     * (De-)activeer de verlichting.
     *
     * @param enabled Of de verlichting actief moet zijn.
     */
    public void setVerlichtingEnabled(boolean enabled) {
        sendByte(enabled
                ? COMMAND_VERLICHTING_ACTIVATE
                : COMMAND_VERLICHTING_DEACTIVATE);
    }

    /**
     * Pauzeer en hervat de muziekplayback.
     *
     * @param paused Of de playback gepauzeerd moet worden
     */
    public void setMusicPaused(boolean paused) {
        sendByte(paused
                     ? COMMAND_MUSIC_PAUSE
                     : COMMAND_MUSIC_RESUME);
    }

    /**
     * Laat een nummer verschijnen op het 7-segmentdisplay van de Arduino.
     *
     * @param number Nummer tussen 0-9
     * @throws IllegalArgumentException wanneer opgegeven nummer niet tussen de 0-9 is.
     */
    public void displayNumber(int number) throws IllegalArgumentException {
        // We kunnen alleen maar 0-9 op een 7-segment display laten zien
        if (number < 0 || number > 9) {
            throw new IllegalArgumentException("Nummer moet tussen de 0-9 zijn.");
        }

        sendByte(COMMAND_SET_NUMBER_DISPLAY);
        sendByte((byte) number);
    }

    /**
     * Stuur een muzieknummer naar de Arduino.<br />
     * <br />
     * Beide parameter arrays moeten een gelijke lengte hebben.
     *
     * @param toonhoogtes De frequenties van de tonen
     * @param toonlengtes De 1/lengte van de tonen (1 = 1 sec, 2 = 0.5 sec, 4 = 0.25 sec)
     */
    public void sendMusic(MusicNote[] toonhoogtes, byte[] toonlengtes) throws IllegalArgumentException {
        if (toonhoogtes.length != toonlengtes.length) {
            throw new IllegalArgumentException("Toonhoogtes en toonlengtes arrays zijn niet even lang");
        }

        // Buffer voor het commando + de muziekdata
        byte[] data = new byte[1 + 1 + (toonhoogtes.length * 2) + toonhoogtes.length];
        int index = 0;

        // Stuur het commando "106" naar de Arduino en laat hiermee weten dat we muziek sturen.
        data[index++] = COMMAND_SEND_MUSIC_DATA;

        // Stuur de lengte van het muzieknummer naar de Arduino
        data[index++] = (byte)(toonhoogtes.length);

        // Stuur de frequentie van elke toon als 16-bit unsigned int naar de Arduino
        for (MusicNote toonhoogte : toonhoogtes) {
            int frequency = toonhoogte.getFrequency();
            data[index++] = (byte) frequency;
            data[index++] = (byte) (frequency >> 8);
        }

        // Stuur de lengte van elke toon als 8-bit unsigned int naar de Arduino.
        for (byte toonlengte : toonlengtes) {
            data[index++] = toonlengte;
        }

        // Stuur alle bytes en flush de output stream
        sendBytes(data);
    }

    /**
     * Parser functie voor muzieknoten uit "659:4,784:2" formaat.<br />
     * <br />
     * Updatet ook de progress bar van de <i>centerPanel</i>.
     *
     * @param centerPanel Het paneel waarvan de progress bar geupdatet moet worden.
     */
    public void sendMusic(CenterPanel centerPanel, String notes) {
        String[] notesArray = notes.split(",");

        MusicNote[] outputNotes = new MusicNote[notesArray.length];
        byte[] outputDurations = new byte[notesArray.length];
        int outputIndex = 0;

        for (String note : notesArray) {
            String[] noteData = note.split(":");

            if (noteData.length < 2) {
                throw new RuntimeException("Note data is ongeldig");
            }

            outputNotes[outputIndex] = MusicNote.fromString(noteData[0]);
            outputDurations[outputIndex++] = Byte.parseByte(noteData[1]);
        }

        sendMusic(outputNotes, outputDurations);

        // set the progress bar values on the MainScreen
        centerPanel.resetProgressBar(outputDurations);
    }
}
