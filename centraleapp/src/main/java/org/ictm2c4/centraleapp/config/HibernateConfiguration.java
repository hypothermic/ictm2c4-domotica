package org.ictm2c4.centraleapp.config;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Een configuration met alle beans welke we nodig hebben voor Hibernate.
 */
@Configuration
@EnableTransactionManagement
public class HibernateConfiguration {

    private static final Properties HIBERNATE_PROPERTIES = new Properties();

    /**
     * @return De session factory die kan worden gebruikt vanuit een Data Access Object (DAO)
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("org.ictm2c4.centraleapp.model");
        sessionFactory.setHibernateProperties(HIBERNATE_PROPERTIES);

        return sessionFactory;
    }

    /**
     * @return De data source voor de database connectie met alle juiste parameters.
     */
    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://db.hypothermic.nl:3306/Domotica?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        dataSource.setUsername("ictm2c4-public");
        dataSource.setPassword("Z3Ee!bz7M|+w*3uY7-<[f=qeFM><&v%=cUK1~Clf__R\"g69.={V@F]I{yrW=CE}-Q");

        return dataSource;
    }

    /**
     * @return De transactions manager die door Hibernate intern wordt gebruikt.
     */
    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    /*
     * Initialiseer de Session Factory met de juiste Hibernate settings.
     */
    static {
        HIBERNATE_PROPERTIES.setProperty("hibernate.hbm2ddl.auto", "update");
        HIBERNATE_PROPERTIES.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL57InnoDBDialect");
    }
}
