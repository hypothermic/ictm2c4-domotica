package org.ictm2c4.centraleapp;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.model.Track;
import org.ictm2c4.centraleapp.persistance.PlaylistDao;
import org.ictm2c4.centraleapp.persistance.PlaylistLineDao;
import org.ictm2c4.centraleapp.persistance.TrackDao;
import org.ictm2c4.centraleapp.ui.CustomButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AddTrackDialog extends JDialog implements ActionListener {

    private JPanel topPanel, mainPanel, trackPanel;
    private JLabel title, description, trackPaginationLabel;
    private CustomButton backButton, saveButton, nextPageButton, prevPageButton;
    private TrackDao trackDao;
    private PlaylistDao playlistDao;
    private PlaylistLineDao playlistLineDao;
    private Playlist playlist;
    private ArrayList<Track> trackList = new ArrayList<>();
    private Profile profile;

    // format for the duration of the tracks
    private DecimalFormat trackDurationFormat = new DecimalFormat("00.00");

    private int page = 0;
    private long playlistId;
    private boolean playlistDeleted = false;

    public AddTrackDialog(JFrame parent, TrackDao trackDao, Playlist playlist, PlaylistDao playlistDao,
                          PlaylistLineDao playlistLineDao, Profile profile, long playlistId) {
        this.trackDao = trackDao;
        this.playlistLineDao = playlistLineDao;
        this.playlistDao = playlistDao;
        this.playlist = playlist;
        this.profile = profile;
        this.playlistId = playlistId;

        trackList.addAll(playlistDao.getAllTracks(playlist.getId()));

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        setTitle("Track toevoegen");
        setAlwaysOnTop(true);
        setPreferredSize(new Dimension(700, 750));
        setResizable(false);

        createTopPanel();
        createMainPanel();

        setVisible(true);
        pack();
        // place dialog in the center of the MainScreen
        setLocationRelativeTo(parent);
    }

    private void createTopPanel() {
        topPanel = new JPanel();
        topPanel.setLayout(new GridBagLayout());
        topPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        topPanel.setBackground(MainScreen.secondaryBackground);

        GridBagConstraints constraints = new GridBagConstraints();
        // set the padding for the components added to topPanel
        constraints.insets = new Insets(10, 10, 10, 10);
        // make sure the components don't clump together in the center
        constraints.weightx = 1;
        constraints.gridx = 0;
        constraints.anchor = GridBagConstraints.NORTH;

        backButton = new CustomButton("Annuleren");
        backButton.addActionListener(this);
        topPanel.add(backButton, constraints);

        constraints.gridy = 0;
        constraints.gridx = 1;

        addTitleAndDescription();

        constraints.gridx = 2;
        saveButton = new CustomButton("Opslaan");
        saveButton.addActionListener(this);
        topPanel.add(saveButton, constraints);

        // add the topPanel to the top of the JFrame
        add(topPanel, BorderLayout.PAGE_START);
    }

    private void createMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setBackground(MainScreen.primaryBackground);

        createTrackPanel();
        addAllTracks();

        GridBagConstraints constraints = new GridBagConstraints();
        // set the padding for the components added to mainPanel
        constraints.insets = new Insets(10, 10, 10, 10);
        // make sure the components don't clump together in the center
        constraints.weightx = 1;

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.SOUTHWEST;
        prevPageButton = new CustomButton("Vorige");
        prevPageButton.setFont(new Font(null, Font.PLAIN, 13));
        prevPageButton.addActionListener(this);
        mainPanel.add(prevPageButton, constraints);

        // add the pagination label
        // text will be added in the setPageButtons function
        constraints.gridx = 1;
        constraints.anchor = GridBagConstraints.SOUTH;
        trackPaginationLabel = new JLabel();
        trackPaginationLabel.setFont(new Font(null, Font.PLAIN, 13));
        mainPanel.add(trackPaginationLabel, constraints);

        constraints.gridx = 2;
        constraints.anchor = GridBagConstraints.SOUTHEAST;
        nextPageButton = new CustomButton("Volgende");
        nextPageButton.setFont(new Font(null, Font.PLAIN, 13));
        nextPageButton.addActionListener(this);
        mainPanel.add(nextPageButton, constraints);

        setPageButtons();

        // add the mainPanel to the JFrame
        add(mainPanel, BorderLayout.CENTER);
    }

    private void createTrackPanel() {
        trackPanel = new JPanel();
        trackPanel.setBackground(MainScreen.primaryBackground);

        trackPanel.setLayout(new GridBagLayout());
    }

    private void addAllTracks() {
        trackPanel.removeAll();

        Color[] buttonColors = new Color[]{new Color(156, 159, 217), new Color(180, 182, 219)};

        GridBagConstraints constraints = new GridBagConstraints();
        // set the padding for the components added to topPanel
        constraints.insets = new Insets(10, 10, 5, 10);
        // make sure the components don't clump together in the center
        constraints.weightx = 1;
        constraints.weighty = 1;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = GridBagConstraints.NORTH;

        List<Track> allTracks = trackDao.getAllTracks(page * 7, 7);

        // for a max of 7 tracks, add them to the panel
        for (int i = 7 * page; i < Math.min(trackDao.getTotalNumberOfTracks(), 7 * page + 7); i++) {
            constraints.gridy += 1;
            JButton trackButton = new JButton();
            trackButton.setBackground(buttonColors[i % 2]);
            trackButton.setForeground(Color.WHITE);

            JCheckBox checkBox = new JCheckBox();

            // custom ActionListener for each button
            int trackId = i % 7;
            trackButton.addActionListener(e -> {
                // (un)select the checkbox associated with the button and add/remove the track
                checkBox.setSelected(!checkBox.isSelected());
                trackButtonAction(checkBox, allTracks, trackId);
            });

            trackButton.setLayout(new GridBagLayout());
            GridBagConstraints trackButtonConstraints = new GridBagConstraints();
            // set the padding for the components added to topPanel
            trackButtonConstraints.insets = new Insets(10, 10, 10, 20);
            // make sure the components don't clump together in the center
            trackButtonConstraints.weightx = 1;

            checkBox.setBackground(buttonColors[i % 2]);
            checkBox.addActionListener(e -> {
                // add/remove the track
                trackButtonAction(checkBox, allTracks, trackId);
            });
            trackButton.add(checkBox);

            JLabel trackName = new JLabel(allTracks.get(trackId).getTitle() + " - " + allTracks.get(trackId).getArtist());
            trackName.setFont(new Font(null, Font.PLAIN, 15));
            trackButtonConstraints.anchor = GridBagConstraints.WEST;
            trackButton.add(trackName, trackButtonConstraints);

            // format the track duration
            String formattedDuration = trackDurationFormat.format(allTracks.get(trackId).getDuration());
            JLabel duration = new JLabel(formattedDuration.replace(".", ":"));
            duration.setFont(new Font(null, Font.PLAIN, 15));
            trackButtonConstraints.anchor = GridBagConstraints.EAST;
            trackButton.add(duration, trackButtonConstraints);

            trackPanel.add(trackButton, constraints);

            if (trackList.contains(trackDao.findTrack(i))) {
                checkBox.setSelected(true);
            }
        }

        mainPanel.add(trackPanel, constraints);
    }

    private void trackButtonAction(JCheckBox checkbox, List<Track> allTracks, int trackId) {
        if (checkbox.isSelected()) {
            trackList.add(allTracks.get(trackId));
        } else {
            trackList.remove(allTracks.get(trackId));
        }
    }

    private void addTitleAndDescription() {
        // Add text fields to enter title and description

        JPanel titleAndDescriptionPanel = new JPanel(new GridBagLayout());
        titleAndDescriptionPanel.setBackground(MainScreen.secondaryBackground);

        GridBagConstraints constraints = new GridBagConstraints();
        // set the padding for the components added to topPanel
        constraints.insets = new Insets(10, 10, 10, 10);
        // make sure the components don't clump together in the center
        constraints.weightx = 1;
        constraints.weighty = 1;

        constraints.gridy = 0;

        title = new JLabel(playlist.getPlaylistName());
        title.setFont(new Font(null, Font.PLAIN, 20));
        title.setPreferredSize(new Dimension(300, 40));
        title.setMinimumSize(new Dimension(300, 40));
        title.setHorizontalAlignment(JTextField.CENTER);
        titleAndDescriptionPanel.add(title, constraints);

        constraints.gridy = 1;

        description = new JLabel(playlist.getPlaylistDescription());
        description.setPreferredSize(new Dimension(300, 80));
        description.setMinimumSize(new Dimension(300, 80));
        titleAndDescriptionPanel.add(description, constraints);

        topPanel.add(titleAndDescriptionPanel);
    }

    private void setPageButtons() {
        // set visibility according to playlistPage value
        prevPageButton.setVisible(page > 0);

        // add text to the pagination label
        trackPaginationLabel.setText("Pagina " + (page + 1) + " van " + (int) Math.ceil(trackDao.getTotalNumberOfTracks() / 7f));

        // set visibility according to playlistPage value and the total number of playlists
        nextPageButton.setVisible(trackDao.getTotalNumberOfTracks() > page * 7 + 7);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == nextPageButton) {
            // go to next page
            if (trackDao.getTotalNumberOfTracks() >= page * 7 + 7) {
                page += 1;
                setPageButtons();
                addAllTracks();
            }
        } else if (e.getSource() == prevPageButton) {
            // go to previous page
            if (page > 0) {
                page -= 1;
                setPageButtons();
                addAllTracks();
            }
        } else if (e.getSource() == backButton) {
            dispose();
        } else if (e.getSource() == saveButton) {
            // check if there is at least one track selected
            if (trackList.isEmpty()) {
                int deletePlaylist = JOptionPane.showConfirmDialog(this, "Afspeellijst moet ten minste één nummer bevatten\n" +
                        "Wilt u deze afspeellijst verwijderen?", "Verwijder afspeellijst?", JOptionPane.YES_NO_OPTION);
                if (deletePlaylist != JOptionPane.NO_OPTION) {
                    playlistLineDao.removeAllTracks((int) playlistId);
                    playlistDao.deletePlaylist(playlist.getId());
                    playlistDeleted = true;
                    JOptionPane.showMessageDialog(this, "Afspeellijst '" + title.getText() + "' succesvol verwijderd");
                    Tools.writeLog("Afspeellijst '" + title.getText() + "' verwijderd");
                    dispose();
                }
                return;
            }

            // save the playlist
            playlistLineDao.removeAllTracks((int) playlistId);
            playlistLineDao.addTracks((int) playlistId, trackList);

            JOptionPane.showMessageDialog(this, "Afspeellijst '" + title.getText() + "' succesvol gewijzigd");
            Tools.writeLog("Afspeellijst '" + title.getText() + "' gewijzigd");
            dispose();
        }
    }

    public boolean isPlaylistDeleted() {
        return playlistDeleted;
    }
}