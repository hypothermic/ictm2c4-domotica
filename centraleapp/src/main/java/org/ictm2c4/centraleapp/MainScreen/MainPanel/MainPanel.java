package org.ictm2c4.centraleapp.MainScreen.MainPanel;

import lombok.Getter;
import org.ictm2c4.centraleapp.MainScreen.MainPanel.PlaylistOverview.PlaylistOverviewPanel;
import org.ictm2c4.centraleapp.MainScreen.MainPanel.TrackOverview.TrackOverviewPanel;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Track;
import org.jboss.jandex.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.primaryBackground;

@Getter
@Component
@Scope("prototype")
public class MainPanel extends JPanel {

    private PlaylistOverviewPanel playlistOveviewPanel;
    private TrackOverviewPanel trackOverviewPanel;

    private MainScreen mainScreen;

    @Autowired
    private ApplicationContext applicationContext;

    public MainPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new GridBagLayout());
        setBackground(primaryBackground);

        return this;
    }

    public void showPlaylistsOverview() {
        removeAll();

        playlistOveviewPanel = applicationContext.getBean(PlaylistOverviewPanel.class).create(mainScreen);

        add(playlistOveviewPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10),
                0, 0));

        updateUI();
        repaint();
    }

    public void showSelectedPlaylist(Playlist playlist) {
        removeAll();

        trackOverviewPanel = applicationContext.getBean(TrackOverviewPanel.class).create(mainScreen, playlist);

        add(trackOverviewPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10),
                0, 0));

        updateUI();
        repaint();
    }

    public long getNumberOfTracks(int playlistId) {
        return mainScreen.getPlaylistDao().getNumberOfTracks(playlistId);
    }
}
