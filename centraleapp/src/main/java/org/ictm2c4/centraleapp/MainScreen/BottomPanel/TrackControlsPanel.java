package org.ictm2c4.centraleapp.MainScreen.BottomPanel;

import lombok.Getter;
import lombok.Setter;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.MusicNote;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.Track;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@Setter
@Component
@Scope("prototype")
public class TrackControlsPanel extends JPanel implements ActionListener {

    private MainScreen mainScreen;

    private CustomButton prevTrackButton,
            pauzeTrackButton, nextTrackButton;
    private boolean playing = false;

    @Autowired
    private ApplicationContext applicationContext;

    public TrackControlsPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new FlowLayout());
        setBackground(new Color(182, 184, 219));

        prevTrackButton = new CustomButton("back.png", 20, 20);
        prevTrackButton.addActionListener(this);
        add(prevTrackButton);

        pauzeTrackButton = new CustomButton("play-button.png", 20, 20);
        pauzeTrackButton.addActionListener(this);
        add(pauzeTrackButton);

        nextTrackButton = new CustomButton("next-1.png", 20, 20);
        nextTrackButton.addActionListener(this);
        add(nextTrackButton);

        return this;
    }

    /**
     * Sets the image displayed in the play/pause button
     * Pauses/resumes the music
     * Writes into the log file
     */
    public void setPlayPauseButton() {
        if (playing) {
            pauzeTrackButton.setIcon("pause.png");
            mainScreen.getArduino().setMusicPaused(false);
            mainScreen.getBottomPanel().getCenterPanel().getTimer().start();
            Tools.writeLog("Muziek aangezet");
        } else {
            pauzeTrackButton.setIcon("play-button.png");
            mainScreen.getArduino().setMusicPaused(true);
            mainScreen.getBottomPanel().getCenterPanel().getTimer().stop();
            Tools.writeLog("Muziek gepauzeerd");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pauzeTrackButton) {
            // pauze/play track
            playing = !playing;
            setPlayPauseButton();

            if (playing) {
                mainScreen.getBottomPanel().getCenterPanel().resetProgressBar();
            }

        } else if (e.getSource() == prevTrackButton || e.getSource() == nextTrackButton) {
            playing = true;
            setPlayPauseButton();

            List<Track> allTracks = mainScreen.getMainPanel().getTrackOverviewPanel().getTrackPanel().getAllTracks();
            int currentTrack = mainScreen.getBottomPanel().getLeftPanel().getCurrentTrack().getId();

            int newTrackId = (e.getSource() == prevTrackButton ? currentTrack - 1 : currentTrack + 1) % 4;
            Track newTrack = allTracks.get(newTrackId);

            mainScreen.getArduino().sendMusic(mainScreen.getBottomPanel().getCenterPanel(), newTrack.getNotes());

            mainScreen.getBottomPanel().getLeftPanel().setCurrentTrack(newTrack);
            mainScreen.getBottomPanel().getLeftPanel().updateTitleAndArtist();
        }
    }
}
