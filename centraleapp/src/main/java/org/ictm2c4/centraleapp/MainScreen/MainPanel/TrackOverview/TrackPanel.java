package org.ictm2c4.centraleapp.MainScreen.MainPanel.TrackOverview;

import lombok.Getter;
import org.ictm2c4.centraleapp.MainScreen.BottomPanel.LeftPanel;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.MusicNote;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.arduino.Arduino;
import org.ictm2c4.centraleapp.arduino.MusicArduino;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.model.Track;
import org.jboss.jandex.Main;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.List;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.primaryBackground;

@Getter
@Component
@Scope("prototype")
public class TrackPanel extends JPanel {

    private MainScreen mainScreen;

    private List<Track> allTracks;

    // format for the duration of the tracks
    private DecimalFormat trackDurationFormat = new DecimalFormat("00.00");

    public TrackPanel create(MainScreen mainScreen, Playlist currentPlaylist, int trackPage) {
        this.mainScreen = mainScreen;

        setBackground(primaryBackground);

        setLayout(new GridBagLayout());

        Color[] trackColors = new Color[]{new Color(156, 159, 217), new Color(180, 182, 219)};

        allTracks = mainScreen.getPlaylistDao().getAllTracks(currentPlaylist.getId(), trackPage * 8, 8);

        int gridY = 0;

        // for a max of 8 tracks, add them to the panel
        for (int i = 8 * trackPage; i < Math.min(mainScreen.getMainPanel().getNumberOfTracks(currentPlaylist.getId()),
                8 * trackPage + 8); i++) {
            gridY += 1;
            JButton trackButton = new JButton();
            trackButton.setBackground(trackColors[i % 2]);
            trackButton.setForeground(Color.WHITE);

            int trackId = i % 8;

            // if the button gets pressed, play the song
            trackButton.addActionListener(e -> {
                Track currentTrack   = allTracks.get(trackId);
                MusicArduino arduino = mainScreen.getArduino();
                LeftPanel leftPanel  = mainScreen.getBottomPanel().getLeftPanel();

                // update the leftpanel in BottomPanel
                leftPanel.setCurrentTrack(currentTrack);
                leftPanel.updateTitleAndArtist();

                // Notes worden als "659:4,784:2" etc... opgeslagen, of NULL als er geen data beschikbaar is.
                String notes = currentTrack.getNotes();
                if (notes != null && notes.length() > 0) {
                    // send music to the arduino and start playing music
                    arduino.sendMusic(mainScreen.getBottomPanel().getCenterPanel(), notes);
                    arduino.setMusicPaused(false);
                    mainScreen.getBottomPanel().getCenterPanel().getTrackControls().setPlaying(true);
                    mainScreen.getBottomPanel().getCenterPanel().getTrackControls().setPlayPauseButton();
                }

                // write loglline to log file
                Tools.writeLog("Track '"  + currentTrack.getTitle() + "' van " + currentTrack.getArtist() + " afspelen");
            });

            trackButton.setLayout(new GridBagLayout());
            GridBagConstraints trackConstraints = new GridBagConstraints();
            // set the padding for the components added to topPanel
            trackConstraints.insets = new Insets(10, 10, 10, 20);
            // make sure the components don't clump together in the center
            trackConstraints.weightx = 1;

            // Add the name of the track to the button
            JLabel trackName = new JLabel(allTracks.get(trackId).getTitle() + " - " + allTracks.get(trackId).getArtist());
            trackName.setFont(new Font(null, Font.PLAIN, 15));
            trackConstraints.anchor = GridBagConstraints.WEST;
            trackButton.add(trackName, trackConstraints);

            // format the track duration
            String formattedDuration = trackDurationFormat.format(allTracks.get(trackId).getDuration());

            // Add the track duration to the button
            JLabel duration = new JLabel(formattedDuration.replace(".", ":"));
            duration.setFont(new Font(null, Font.PLAIN, 15));
            trackConstraints.anchor = GridBagConstraints.EAST;
            trackButton.add(duration, trackConstraints);

            // Add the button to the panel
            add(trackButton, new GridBagConstraints(0, gridY, 3, 1, 1, 1,
                    GridBagConstraints.PAGE_START, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                    0, 0));
        }

        return this;
    }
}
