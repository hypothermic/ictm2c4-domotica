package org.ictm2c4.centraleapp.MainScreen.MainPanel.PlaylistOverview;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.model.Playlist;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.primaryBackground;

@Component
@Scope("prototype")
public class PlaylistPanel extends JPanel {

    private MainScreen mainScreen;

    public PlaylistPanel create(MainScreen mainScreen, List<Playlist> playlists, long numberOfPlaylists, int playlistPage) {
        this.mainScreen = mainScreen;

        setBackground(primaryBackground);

        setLayout(new GridBagLayout());

        if (numberOfPlaylists == 0) {
            JLabel noPlaylists = (new JLabel("Uw heeft nog geen afspeellijsten"));
            noPlaylists.setFont(new Font(null, Font.PLAIN, 35));

            add(noPlaylists, new GridBagConstraints(0, 0, 3, 1, 1, 1,
                    GridBagConstraints.PAGE_START, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                    0, 0));
        } else {
            addPlaylistsToPanel(playlists, numberOfPlaylists, playlistPage);
        }

        setVisible(true);

        return this;
    }

    private void addPlaylistsToPanel(List<Playlist> playlists, long numberOfPlaylists, int playlistPage) {
        Color[] playlistColors = new Color[]{new Color(156, 159, 217), new Color(180, 182, 219)};

        int gridY = 0;

        // for a max of 8 playlists, add them to the panel
        for (Playlist playlist : playlists) {
            gridY += 1;
            JButton playlistButton = new JButton();
            playlistButton.setBackground(playlistColors[gridY % 2]);

            int playlistID = playlist.getId();

            // If the button gets pressed, open the track overview panel
            playlistButton.addActionListener(e -> mainScreen.getMainPanel().showSelectedPlaylist(playlist));

            playlistButton.setLayout(new GridBagLayout());

            // Add the name of the playlist to the button
            JLabel playlistName = new JLabel(playlist.getPlaylistName());
            playlistName.setFont(new Font(null, Font.PLAIN, 15));

            playlistButton.add(playlistName, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                    GridBagConstraints.WEST, 0, new Insets(10, 10, 10, 10), 0, 0));

            // Add the playlist description to the button
            String text = playlist.getPlaylistDescription();
            JLabel playlistDescription = new JLabel(text.substring(0, Math.min(text.length(), 100)));
            playlistDescription.setFont(new Font(null, Font.PLAIN, 15));

            playlistButton.add(playlistDescription, new GridBagConstraints(1, 0, 1, 1, 1, 1,
                    GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 0, 0));

            // Add the number of tracks in the playlist to the button
            JLabel numberOfTracks = new JLabel(String.valueOf(mainScreen.getMainPanel().getNumberOfTracks(playlistID)));
            numberOfTracks.setFont(new Font(null, Font.PLAIN, 15));

            playlistButton.add(numberOfTracks, new GridBagConstraints(2, 0, 1, 1, 1, 1,
                    GridBagConstraints.EAST, 0, new Insets(10, 10, 10, 10), 0, 0));

            // Add the button to the panel
            add(playlistButton, new GridBagConstraints(0, gridY, 3, 1, 1, 1,
                    GridBagConstraints.PAGE_START, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                    0, 0));
        }
    }
}
