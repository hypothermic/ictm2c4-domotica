package org.ictm2c4.centraleapp.MainScreen.BottomPanel;

import lombok.Getter;
import lombok.Setter;
import org.ictm2c4.centraleapp.MainScreen.MainPanel.MainPanel;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.ui.BottomPanelLayoutManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.secondaryBackground;

@Getter
@Setter
@Component
@Scope("prototype")
public class BottomPanel extends JPanel  {

    private LeftPanel leftPanel;
    private CenterPanel centerPanel;
    private RightPanel rightPanel;

    private MainScreen mainScreen;

    @Autowired
    private ApplicationContext applicationContext;

    public BottomPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new BottomPanelLayoutManager());
        setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));
        setBackground(secondaryBackground);

        // create three additional panels to hold the content
        leftPanel = applicationContext.getBean(LeftPanel.class);
        centerPanel = applicationContext.getBean(CenterPanel.class).create(mainScreen);
        rightPanel = applicationContext.getBean(RightPanel.class).create(mainScreen);

        add(leftPanel, BottomPanelLayoutManager.LEFT);
        add(centerPanel, BottomPanelLayoutManager.CENTERED);
        add(rightPanel, BottomPanelLayoutManager.RIGHT);

        return this;
    }
}
