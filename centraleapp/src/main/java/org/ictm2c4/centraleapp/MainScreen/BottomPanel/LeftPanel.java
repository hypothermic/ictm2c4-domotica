package org.ictm2c4.centraleapp.MainScreen.BottomPanel;

import lombok.Getter;
import org.ictm2c4.centraleapp.model.Track;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.secondaryBackground;

@Getter
@Component
@Scope("prototype")
public class LeftPanel extends JPanel {

    private JLabel trackName, artistName;
    private Track currentTrack = null;

    @PostConstruct
    public void init() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));
        setBackground(secondaryBackground);

        // add artist and track names to the left side of the panel
        trackName = new JLabel(currentTrack != null ? currentTrack.getTitle() : "-");
        trackName.setFont(new Font(null, Font.PLAIN, 25));

        add(trackName, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.WEST, 0, new Insets(10, 10, 0, 10), 0, 0));

        artistName = new JLabel(currentTrack != null ? currentTrack.getArtist() : "-");
        artistName.setFont(new Font(null, Font.PLAIN, 15));

        add(artistName, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.WEST, 0, new Insets(5, 10, 10, 10), 0, 0));
    }

    public void updateTitleAndArtist() {
        trackName.setText(currentTrack != null ? currentTrack.getTitle() : "-");
        artistName.setText(currentTrack != null ? currentTrack.getArtist() : "-");
    }

    public void setCurrentTrack(Track currentTrack) {
        this.currentTrack = currentTrack;
    }

}
