package org.ictm2c4.centraleapp.MainScreen;

import lombok.Getter;
import lombok.Setter;
import org.ictm2c4.centraleapp.MainScreen.BottomPanel.BottomPanel;
import org.ictm2c4.centraleapp.MainScreen.MainPanel.MainPanel;
import org.ictm2c4.centraleapp.MainScreen.TopPanel.TopPanel;
import org.ictm2c4.centraleapp.arduino.MusicArduino;
import org.ictm2c4.centraleapp.model.Profile;
import org.ictm2c4.centraleapp.persistance.PlaylistDao;
import org.ictm2c4.centraleapp.persistance.PlaylistLineDao;
import org.ictm2c4.centraleapp.persistance.ProfileDao;
import org.ictm2c4.centraleapp.persistance.TrackDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Het hoofdscherm met o.a. de playlists en omgevingsinfo.
 */
@Component
@Getter
@Setter
@SuppressWarnings({"FieldCanBeLocal"})
@Scope("prototype")
public class MainScreen extends JFrame {

    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private PlaylistDao playlistDao;

    @Autowired
    private PlaylistLineDao playlistLineDao;

    @Autowired
    private TrackDao trackDao;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("getActiveProfile")
    private int profileId;

    private TopPanel topPanel;
    private MainPanel mainPanel;
    private BottomPanel bottomPanel;
    public static Color
            buttonColor = new Color(110, 115, 196),
            primaryBackground = new Color(223, 224, 247),
            secondaryBackground = new Color(182, 184, 219);
    public static Font mainFont = new Font(null, Font.PLAIN, 20);

    @Getter
    private volatile Profile profile;

    private MusicArduino arduino;

    @PostConstruct
    public void init() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        profile = profileDao.findProfile(profileId);

        arduino = new MusicArduino(this);

        createContentPanels();

        pack();

        // set the minimum size for the window
        setMinimumSize(getSize());
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                Dimension currentSize = getSize();
                Dimension minimumSize = getMinimumSize();
                if (currentSize.width < minimumSize.width) {
                    currentSize.width = minimumSize.width;
                }
                if (currentSize.height < minimumSize.height) {
                    currentSize.height = minimumSize.height;
                }
                setSize(currentSize);
            }
        });

        // maximize the application
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    /**
     * Create the different subpanels within MainScreen and add them
     */
    private void createContentPanels() {
        topPanel = applicationContext.getBean(TopPanel.class).create(this);
        mainPanel = applicationContext.getBean(MainPanel.class).create(this);
        bottomPanel = applicationContext.getBean(BottomPanel.class).create(this);

        // add the topPanel to the top of the JFrame
        add(topPanel, BorderLayout.PAGE_START);
        // add the mainPanel to the center of the JFrame
        add(mainPanel, BorderLayout.CENTER);
        // add the bottomPanel to the bottom of the JFrame
        add(bottomPanel, BorderLayout.PAGE_END);

        mainPanel.showPlaylistsOverview();
    }
}