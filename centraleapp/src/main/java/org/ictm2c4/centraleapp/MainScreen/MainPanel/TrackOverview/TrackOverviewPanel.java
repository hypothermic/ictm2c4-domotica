package org.ictm2c4.centraleapp.MainScreen.MainPanel.TrackOverview;

import lombok.Getter;
import org.ictm2c4.centraleapp.AddTrackDialog;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.primaryBackground;

@Getter
@Component
@Scope("prototype")
public class TrackOverviewPanel extends JPanel implements ActionListener {

    private MainScreen mainScreen;

    private CustomButton addTrackButton, prevTrackPageButton, nextTrackPageButton, backToOverviewButton;

    private int trackPage = 0;

    private JLabel trackPaginationLabel;

    private Playlist currentPlaylist;

    private TrackPanel trackPanel;

    @Autowired
    private ApplicationContext applicationContext;

    public TrackOverviewPanel create(MainScreen mainScreen, Playlist playlist) {
        this.mainScreen = mainScreen;
        this.currentPlaylist = playlist;

        setLayout(new GridBagLayout());

        setBackground(primaryBackground);

        addTracksToPanel();

        currentPlaylist = playlist;

        backToOverviewButton = new CustomButton("Terug naar overzicht");
        backToOverviewButton.setFont(new Font(null, Font.PLAIN, 15));
        backToOverviewButton.addActionListener(this);
        add(backToOverviewButton, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTHWEST, 0, new Insets(10, 20, 0, 20), 0, 0));

        JLabel playlistName = new JLabel(playlist.getPlaylistName());
        playlistName.setFont(new Font(null, Font.PLAIN, 20));
        add(playlistName, new GridBagConstraints(1, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTH, 0, new Insets(10, 20, 0, 20), 0, 0));

        addTrackButton = new CustomButton("plus.png", 10, 10, "Track toevoegen/verwijderen");
        addTrackButton.setFont(new Font(null, Font.PLAIN, 15));
        addTrackButton.addActionListener(this);
        add(addTrackButton, new GridBagConstraints(2, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTHEAST, 0, new Insets(10, 20, 0, 20), 0, 0));

        addPagination();
        setTrackPageButtons();

        return this;
    }

    private void addPagination() {
        prevTrackPageButton = new CustomButton("Vorige");
        prevTrackPageButton.setFont(new Font(null, Font.PLAIN, 13));
        prevTrackPageButton.addActionListener(this);
        add(prevTrackPageButton, new GridBagConstraints(0, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTHWEST, 0, new Insets(10, 20, 0, 20), 0, 0));

        // add the pagination label
        // text will be added in the setTrackPageButtons function
        trackPaginationLabel = new JLabel();
        trackPaginationLabel.setFont(new Font(null, Font.PLAIN, 13));
        add(trackPaginationLabel, new GridBagConstraints(1, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTH, 0, new Insets(10, 20, 0, 20), 0, 0));

        nextTrackPageButton = new CustomButton("Volgende");
        nextTrackPageButton.setFont(new Font(null, Font.PLAIN, 13));
        nextTrackPageButton.addActionListener(this);
        add(nextTrackPageButton, new GridBagConstraints(2, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTHEAST, 0, new Insets(10, 20, 0, 20), 0, 0));
    }

    /**
     * Adds the panel that shows the tracks
     */
    private void addTracksToPanel() {
        if (trackPanel != null) remove(trackPanel);
        trackPanel = applicationContext.getBean(TrackPanel.class).create(mainScreen, currentPlaylist, trackPage);
        add(trackPanel, new GridBagConstraints(0, 1, 3, 1, 1, 1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                0, 0));
        updateUI();
    }

    private void setTrackPageButtons() {
        // set visibility according to trackPage value
        prevTrackPageButton.setVisible(trackPage > 0);

        // add text to the pagination label
        trackPaginationLabel.setText("Pagina " + (trackPage + 1) + " van " +
                (int) Math.ceil(mainScreen.getMainPanel().getNumberOfTracks(currentPlaylist.getId()) / 8f));

        // set visibility according to trackPage value and the total number of tracks
        nextTrackPageButton.setVisible(mainScreen.getMainPanel().getNumberOfTracks(currentPlaylist.getId()) > trackPage * 8 + 8);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == nextTrackPageButton) {
            // go to next page
            if (mainScreen.getMainPanel().getNumberOfTracks(currentPlaylist.getId()) >= trackPage * 8 + 8) {
                trackPage += 1;
                setTrackPageButtons();
                addTracksToPanel();
            }
        } else if (e.getSource() == prevTrackPageButton) {
            // go to previous page
            if (trackPage > 0) {
                trackPage -= 1;
                setTrackPageButtons();
                addTracksToPanel();
            }
        } else if (e.getSource() == backToOverviewButton) {
            // go back to the playlist overview
            removeAll();
            mainScreen.getMainPanel().showPlaylistsOverview();
        } else if (e.getSource() == addTrackButton) {
            AddTrackDialog addTrackDialog = new AddTrackDialog(mainScreen, mainScreen.getTrackDao(), currentPlaylist,
                    mainScreen.getPlaylistDao(), mainScreen.getPlaylistLineDao(), mainScreen.getProfile(), currentPlaylist.getId());
            addTrackDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);

                    // redraw mainPanel
                    removeAll();
                    if (addTrackDialog.isPlaylistDeleted()) {
                        mainScreen.getMainPanel().showPlaylistsOverview();
                    } else {
                        mainScreen.getMainPanel().showSelectedPlaylist(currentPlaylist);
                    }
                }
            });
        }
    }
}
