package org.ictm2c4.centraleapp.MainScreen.BottomPanel;

import lombok.Getter;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.secondaryBackground;

@Component
@Scope("prototype")
@Getter
public class CenterPanel extends JPanel {

    private MainScreen mainScreen;
    private TrackControlsPanel trackControls;
    private JSlider progressBar;
    private JLabel timeLeftLabel;
    private Timer timer;
    private int duration;

    @Autowired
    private ApplicationContext applicationContext;

    public CenterPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new GridBagLayout());
        setBackground(secondaryBackground);

        // add the control buttons and progress bar to the center of the panel
        trackControls = applicationContext.getBean(TrackControlsPanel.class).create(mainScreen);
        add(trackControls, new GridBagConstraints(0, 0, 2, 1,
                1, 1, GridBagConstraints.CENTER, 0, new Insets(10, 10, 0, 10),
                0, 0));

        progressBar = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
        progressBar.setEnabled(false);
        progressBar.setPaintTrack(true);
        progressBar.setBackground(new Color(182, 184, 219));
        add(progressBar, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0),
                0, 0));


        timeLeftLabel = new JLabel("-:-");
        add(timeLeftLabel, new GridBagConstraints(1, 1, 1, 1, 1, 1,
                GridBagConstraints.EAST, 0, new Insets(0, 5, 10, 0), 0, 0));

        return this;
    }

    public void resetProgressBar(byte[] noteLenghts) {
        if(timer != null) timer.stop();

        float durationAsFloat = 0;
        for (byte noteLength : noteLenghts) {
            durationAsFloat += 1f / noteLength;
        }
        duration = (int) Math.ceil(durationAsFloat);

        this.resetProgressBar();
    }

    public void resetProgressBar() {
        progressBar.setMaximum(duration);
        progressBar.setValue(0);
        timeLeftLabel.setText(Integer.toString(duration));

        // set and start the timer for the progressbar and label
        setTimer((int) Math.ceil(duration));
        timer.start();
    }

    private void setTimer(int duration) {
        timer = new Timer(1000, e -> {
            // update the progressbar
            progressBar.setValue(progressBar.getValue() + 1);
            int timeLeft = duration - progressBar.getValue();

            // shows time in seconds
            timeLeftLabel.setText(Integer.toString(timeLeft));
            updateUI();

            if(timeLeft <= 0) {
                timer.stop();
                trackControls.setPlaying(false);
                trackControls.setPlayPauseButton();
            }
        });
    }
}
