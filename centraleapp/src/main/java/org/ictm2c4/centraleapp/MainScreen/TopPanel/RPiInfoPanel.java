package org.ictm2c4.centraleapp.MainScreen.TopPanel;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.Tools.Tools;
import org.ictm2c4.centraleapp.model.LuchtvochtigheidMeting;
import org.ictm2c4.centraleapp.model.Meting;
import org.ictm2c4.centraleapp.model.TemperatuurMeting;
import org.ictm2c4.centraleapp.scheduler.OmgevingsdataUpdateJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;

@Component
@Scope("prototype")
@CommonsLog
public class RPiInfoPanel extends JPanel {

    private static final JobKey[] JOB_KEYS = new JobKey[] {
            JobKey.jobKey("RPi_Update_Job_Detail"),
            JobKey.jobKey("Arduino_Update_Job_Detail")
    };

    /**
     * De scheduler die de Raspberry Pi info zal updaten
     */
    @Autowired
    @Qualifier("scheduler")
    private Scheduler scheduler;

    @Autowired
    @Qualifier("omgevingsDataUpdateJobDetail")
    private JobDetail omgevingsDataUpdateJobDetail;

    @Autowired
    @Qualifier("lichtwaardeReadJobDetail")
    private JobDetail lichtwaardeReadJobDetail;

    @Autowired
    private ApplicationContext applicationContext;

    @Getter
    private MainScreen mainScreen;

    @Getter
    private final JLabel temperatuurLabel = new JLabel("Temperatuur: 21 °C"),
                         luchtvochtigheidLabel = new JLabel("Luchtvochtigheid: 46%"),
                         lichtintensiteitLabel = new JLabel("Lichtintensiteit 73%");

    public RPiInfoPanel() {
        setLayout(new GridLayout(3, 1));
        setBackground(new Color(182, 184, 219));

        add(temperatuurLabel);
        add(luchtvochtigheidLabel);
        add(lichtintensiteitLabel);
    }

    /**
     * Voer deze code pas uit nadat de Spring dependency injection is uitgevoerd (daarom ook @PostConstruct)
     */
    @PostConstruct
    @SneakyThrows
    public void init() {
        // Start met het updaten van de data
        scheduler.getContext().put(OmgevingsdataUpdateJob.CONTEXT_PANEL_OBJECT_KEY, this);
        scheduler.start();

        // Verwijder alle bestaande jobs
        for (JobKey jobKey : JOB_KEYS) {
            if (scheduler.checkExists(jobKey)) {
                scheduler.deleteJob(jobKey);
            }
        }

        scheduler.scheduleJob(omgevingsDataUpdateJobDetail, applicationContext.getBean("omgevingsDataUpdateTrigger", Trigger.class));
        scheduler.scheduleJob(lichtwaardeReadJobDetail, applicationContext.getBean("lichtwaardeReadTrigger", Trigger.class));
    }

    public void update(Meting meting) {
        if (meting instanceof LuchtvochtigheidMeting) {
            Tools.writeLog("Gemeten luchtvochtigheid: " + meting.getWaarde() + "%");
            this.luchtvochtigheidLabel.setText("Luchtvochtigheid: " + meting.getWaarde() + "%");
        } else if (meting instanceof TemperatuurMeting) {
            Tools.writeLog("Gemeten Temperatuur: " + meting.getWaarde() + " °C");
            this.temperatuurLabel.setText("Temperatuur: " + meting.getWaarde() + " °C");
        }
    }

    @SneakyThrows
    public RPiInfoPanel setMainScreen(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        scheduler.getContext().put(OmgevingsdataUpdateJob.PROFILE_ID_OBJECT_KEY, mainScreen.getProfileId());

        return this;
    }
}
