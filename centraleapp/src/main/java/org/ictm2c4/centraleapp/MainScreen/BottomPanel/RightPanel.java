package org.ictm2c4.centraleapp.MainScreen.BottomPanel;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.profile.screen.ProfileScreen;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.secondaryBackground;

@Component
@Scope("prototype")
public class RightPanel extends JPanel implements ActionListener {
    private CustomButton changeProfileButton;

    @InjectMocks // prevent unnecessary warning
    @Autowired
    private ApplicationContext applicationContext;

    private MainScreen mainScreen;

    public RightPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new GridBagLayout());
        setBackground(secondaryBackground);

        // add the changeProfile button to the right side of the panel
        changeProfileButton = new CustomButton("Ander profiel");
        add(changeProfileButton, new GridBagConstraints(2, 0, 1, 2, 1, 1,
                GridBagConstraints.EAST, 0, new Insets(10, 10, 0, 10), 0, 0));

        changeProfileButton.addActionListener(this);

        return this;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == changeProfileButton) {
            mainScreen.setVisible(false);
            applicationContext.getBean(ProfileScreen.class).setVisible(true);
        }
    }
}
