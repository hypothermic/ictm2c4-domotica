package org.ictm2c4.centraleapp.MainScreen.TopPanel;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.settings.SettingsDialog;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
@Scope("prototype")
public class TopPanel extends JPanel implements ActionListener {

    private CustomButton settingsButton;

    private MainScreen mainScreen;

    @Autowired
    private ApplicationContext applicationContext;

    public TopPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        setBackground(MainScreen.secondaryBackground);

        // create the welcoming text
        JLabel welcomeLabel = new JLabel("Welkom " + mainScreen.getProfile().getNaam());
        welcomeLabel.setFont(new Font(null, Font.PLAIN, 20));

        add(welcomeLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.WEST, 0, new Insets(10, 10, 10, 10), 0, 0));

        add(applicationContext.getBean(RPiInfoPanel.class).setMainScreen(mainScreen), new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 0, 0));

        settingsButton = new CustomButton("Instellingen");
        settingsButton.addActionListener(this);

        add(settingsButton,  new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.EAST, 0, new Insets(10, 10, 10, 10), 0, 0));

        return this;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == settingsButton) {
            applicationContext.getBean(SettingsDialog.class).update().setVisible(true);
        }
    }
}
