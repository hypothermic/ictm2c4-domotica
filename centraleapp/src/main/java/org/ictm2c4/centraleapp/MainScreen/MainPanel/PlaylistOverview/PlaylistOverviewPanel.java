package org.ictm2c4.centraleapp.MainScreen.MainPanel.PlaylistOverview;

import org.ictm2c4.centraleapp.MainScreen.MainScreen;
import org.ictm2c4.centraleapp.NewPlaylistDialog;
import org.ictm2c4.centraleapp.model.Playlist;
import org.ictm2c4.centraleapp.ui.CustomButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

import static org.ictm2c4.centraleapp.MainScreen.MainScreen.primaryBackground;

@Component
@Scope("prototype")
public class PlaylistOverviewPanel extends JPanel implements ActionListener {
    private MainScreen mainScreen;
    private CustomButton nextPlaylistPageButton, prevPlaylistPageButton, addPlaylistButton;
    private JLabel playlistPaginationLabel;

    private long numberOfPlaylists, totalNumberOfPlaylists;

    private List<Playlist> playlists;

    private int playlistPage = 0;

    private PlaylistPanel playlistPanel;

    @Autowired
    private ApplicationContext applicationContext;

    public PlaylistOverviewPanel create(MainScreen mainScreen) {
        this.mainScreen = mainScreen;

        setLayout(new GridBagLayout());

        setBackground(primaryBackground);

        JLabel allPlaylists = new JLabel("Al uw afspeellijsten");
        allPlaylists.setFont(new Font(null, Font.PLAIN, 20));
        add(allPlaylists, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTHWEST, 0, new Insets(10, 20, 0, 20), 0, 0));

        addPlaylistButton = new CustomButton("plus.png", 10, 10, "Afspeellijst toevoegen");
        addPlaylistButton.setFont(new Font(null, Font.PLAIN, 15));
        addPlaylistButton.addActionListener(this);
        add(addPlaylistButton, new GridBagConstraints(2, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTHEAST, 0, new Insets(10, 20, 0, 20), 0, 0));

        setNumberOfPlaylists();
        setPlaylistsList();

        if (numberOfPlaylists == 0) {
            JLabel noPlaylists = (new JLabel("U heeft nog geen afspeellijsten"));
            noPlaylists.setFont(new Font(null, Font.PLAIN, 35));
            add(noPlaylists, new GridBagConstraints(0, 1, 3, 1, 1, 1,
                    GridBagConstraints.PAGE_START, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                    0, 0));
        } else {
            addPlaylistsPanel();
        }

        addPagination();
        setPlaylistPageButtons();

        return this;
    }

    /**
     * Adds the panel that shows the playlists
     */
    private void addPlaylistsPanel() {
        if (playlistPanel != null) remove(playlistPanel);
        playlistPanel = applicationContext.getBean(PlaylistPanel.class).create(mainScreen, playlists, numberOfPlaylists, playlistPage);
        add(playlistPanel, new GridBagConstraints(0, 1, 3, 1, 1, 0,
                GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 5, 10),
                0, 0));
        updateUI();
    }

    private void addPagination() {
        prevPlaylistPageButton = new CustomButton("Vorige");
        prevPlaylistPageButton.setFont(new Font(null, Font.PLAIN, 13));
        prevPlaylistPageButton.addActionListener(this);
        add(prevPlaylistPageButton, new GridBagConstraints(0, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTHWEST, 0, new Insets(10, 20, 0, 20), 0, 0));

        // add the pagination label
        // text will be added in the setPlaylistPageButtons function
        playlistPaginationLabel = new JLabel();
        playlistPaginationLabel.setFont(new Font(null, Font.PLAIN, 13));
        add(playlistPaginationLabel, new GridBagConstraints(1, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTH, 0, new Insets(10, 20, 0, 20), 0, 0));

        nextPlaylistPageButton = new CustomButton("Volgende");
        nextPlaylistPageButton.setFont(new Font(null, Font.PLAIN, 13));
        nextPlaylistPageButton.addActionListener(this);
        add(nextPlaylistPageButton, new GridBagConstraints(2, 2, 1, 1, 1, 1,
                GridBagConstraints.SOUTHEAST, 0, new Insets(10, 20, 0, 20), 0, 0));
    }

    private void setPlaylistPageButtons() {
        // set visibility according to playlistPage value
        prevPlaylistPageButton.setVisible(playlistPage > 0);

        // add text to the pagination label
        playlistPaginationLabel.setText("Pagina " + (playlistPage + 1) + " van " + (int) Math.ceil(numberOfPlaylists / 8f));

        // set visibility according to playlistPage value and the total number of playlists
        nextPlaylistPageButton.setVisible(numberOfPlaylists > playlistPage * 8 + 8);
    }

    private void setNumberOfPlaylists() {
        numberOfPlaylists = mainScreen.getProfileDao().getNumberOfPlaylists(mainScreen.getProfileId());
        totalNumberOfPlaylists = mainScreen.getProfileDao().getTotalNumberOfPlaylists();
    }

    private void setPlaylistsList() {
        playlists = mainScreen.getProfileDao().getAllPlaylists(mainScreen.getProfile().getId(), playlistPage * 8, 8);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addPlaylistButton) {
            JDialog newPlaylistDialog = new NewPlaylistDialog(mainScreen, mainScreen.getTrackDao(),
                    mainScreen.getPlaylistDao(), mainScreen.getPlaylistLineDao(), mainScreen.getProfile(), totalNumberOfPlaylists);
            newPlaylistDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    // update the number of playlists and the List of playlists
                    setNumberOfPlaylists();
                    setPlaylistsList();
                    // redraw mainPanel
                    removeAll();
                    mainScreen.getMainPanel().showPlaylistsOverview();
                    repaint();
                }
            });
        } else if (e.getSource() == nextPlaylistPageButton) {
            // go to next page
            if (numberOfPlaylists >= playlistPage * 8 + 8) {
                playlistPage += 1;
                setPlaylistsList();
                setPlaylistPageButtons();
                addPlaylistsPanel();
            }
        } else if (e.getSource() == prevPlaylistPageButton) {
            // go to previous page
            if (playlistPage > 0) {
                playlistPage -= 1;
                setPlaylistsList();
                setPlaylistPageButtons();
                addPlaylistsPanel();
            }
        }
    }
}
