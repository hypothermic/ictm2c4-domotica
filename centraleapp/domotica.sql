# noinspection SqlNoDataSourceInspectionForFile
#

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

DROP USER IF EXISTS 'ictm2c4-public'@'%';
CREATE USER IF NOT EXISTS 'ictm2c4-public'@'%' IDENTIFIED BY 'Z3Ee!bz7M|+w*3uY7-<[f=qeFM><&v%=cUK1~Clf__R"g69.={V@F]I{yrW=CE}-Q';

SET GLOBAL innodb_file_per_table = 1;
SET GLOBAL innodb_file_format = Barracuda;

CREATE SCHEMA IF NOT EXISTS `Domotica` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `Domotica`;


DROP TABLE IF EXISTS `Domotica`.`Track`;
CREATE TABLE IF NOT EXISTS `Domotica`.`Track`
(
    `TrackID`     BIGINT       NOT NULL,
    `TrackNaam`   VARCHAR(45)  NOT NULL,
    `ArtiestNaam` VARCHAR(100) NOT NULL,
    `Duration`    DOUBLE       NOT NULL,
    `Notes`       VARCHAR(512) NOT NULL,
    PRIMARY KEY (`TrackID`)
) AUTO_INCREMENT = 0
  ROW_FORMAT = COMPRESSED
  ENGINE = InnoDB
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `Domotica`.`Profiel`;
CREATE TABLE IF NOT EXISTS `Domotica`.`Profiel`
(
    `ProfielID`        BIGINT      NOT NULL,
    `Naam`             VARCHAR(64) NOT NULL,
    `Temperatuur`      DOUBLE      NOT NULL,
    `Lichtintensiteit` DOUBLE      NOT NULL,
    PRIMARY KEY (`ProfielID`)
) AUTO_INCREMENT = 0
  ROW_FORMAT = COMPRESSED
  ENGINE = InnoDB
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `Domotica`.`Afspeellijst`;
CREATE TABLE IF NOT EXISTS `Domotica`.`Afspeellijst`
(
    `AfspeellijstID`           BIGINT       NOT NULL,
    `ProfielID`                BIGINT       NOT NULL,
    `AfspeellijstNaam`         VARCHAR(64)  NOT NULL,
    `AfspeellijstBeschrijving` VARCHAR(256) NOT NULL,
    PRIMARY KEY (`AfspeellijstID`),
    #INDEX `fk_Afspeellijst_1_idx` (`ProfielID` ASC) VISIBLE,
    #CONSTRAINT `fk_Afspeellijst_1`
        FOREIGN KEY (`ProfielID`)
            REFERENCES `Domotica`.`Profiel` (`ProfielID`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
) AUTO_INCREMENT = 0
  ROW_FORMAT = COMPRESSED
  ENGINE = InnoDB
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `Domotica`.`AfspeellijstRegel`;
CREATE TABLE IF NOT EXISTS `Domotica`.`AfspeellijstRegel`
(
    `AfspeellijstID` BIGINT NOT NULL,
    `TrackID`        BIGINT NOT NULL,
    PRIMARY KEY (`AfspeellijstID`, `TrackID`),
    #INDEX `fk_AfspeellijstRegel_2_idx` (`TrackID` ASC) VISIBLE,
    #CONSTRAINT `fk_AfspeellijstRegel_1`
        FOREIGN KEY (`AfspeellijstID`)
            REFERENCES `Domotica`.`Afspeellijst` (`AfspeellijstID`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    #CONSTRAINT `fk_AfspeellijstRegel_2`
        FOREIGN KEY (`TrackID`)
            REFERENCES `Domotica`.`Track` (`TrackID`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
) AUTO_INCREMENT = 0
  ROW_FORMAT = COMPRESSED
  ENGINE = InnoDB
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON `Domotica`.`AfspeellijstRegel` TO 'ictm2c4-public'@'%';
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON `Domotica`.`Afspeellijst`      TO 'ictm2c4-public'@'%';
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON `Domotica`.`Profiel`           TO 'ictm2c4-public'@'%';
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON `Domotica`.`Track`             TO 'ictm2c4-public'@'%';


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
